ALTER TABLE USUARIO ADD password VARCHAR2(100) DEFAULT '123456'
create table usuario_rol
(
	id int not null,
	usuario int not null
		constraint usuario_rol_USUARIO_ID_fk
			references USUARIO,
	rol varchar2(50) not null
)

comment on table usuario_rol is 'Contiene los roles de cada usuario'

comment on column usuario_rol.id is 'Identificador de la tabla'

comment on column usuario_rol.usuario is 'id del usuario'

comment on column usuario_rol.rol is 'Rol del usuario'

create unique index usuario_rol_id_uindex
	on usuario_rol (id)

alter table usuario_rol
	add constraint usuario_rol_pk
		primary key (id)

create table rol_permiso
(
	id int not null,
	permiso varchar2(30) not null,
	rol varchar2(30) not null
)

comment on table rol_permiso is 'Contiene los permisos de cada rol'

comment on column rol_permiso.id is 'Identificador de la tabla'

comment on column ROL_PERMISO.PERMISO is 'Identificador del permiso que debe tener cada rol.'

comment on column rol_permiso.rol is 'Contiene el rol del usuario'

create unique index rol_permiso_id_uindex
	on rol_permiso (id)


alter table rol_permiso
	add constraint rol_permiso_pk
		primary key (id)

INSERT INTO "HR"."USUARIO_ROL" ("ID", "USUARIO", "ROL") VALUES (1, 197, 'admin')
INSERT INTO "HR"."ROL_PERMISO" ("ID", "PERMISO", "ROL") VALUES (1, 'agregar_usuario', 'admin')

create table roles
(
	id int not null,
	nombre varchar2(50) not null
)

comment on table roles is 'Roles que pueden ser asignados a los usuarios'

comment on column roles.id is 'Identificador de la tabla'

comment on column roles.nombre is 'Nombre del rol.'

create unique index roles_id_uindex
	on roles (id)

alter table roles
	add constraint roles_pk
		primary key (id)
INSERT INTO "HR"."ROLES" ("ID", "NOMBRE") VALUES (1, 'admin');
INSERT INTO "HR"."ROLES" ("ID", "NOMBRE") VALUES (2, 'dba');
INSERT INTO "HR"."ROLES" ("ID", "NOMBRE") VALUES (3, 'user');

alter table USUARIO_ROL modify ROL int

alter table USUARIO_ROL
	add constraint USUARIO_ROL_ROLES_ID_fk
		foreign key (ROL) references ROLES

alter table USUARIO_ROL drop constraint USUARIO_ROL_USUARIO_ID_FK

alter table USUARIO_ROL
	add constraint USUARIO_ROL_USUARIO_ID_FK
		foreign key (USUARIO) references USUARIO;
INSERT INTO "HR"."USUARIO_ROL" ("ID", "USUARIO", "ROL") VALUES (1, 197, 1);
INSERT INTO "HR"."USUARIO_ROL" ("ID", "USUARIO", "ROL") VALUES (2, 193, 2);
INSERT INTO "HR"."USUARIO_ROL" ("ID", "USUARIO", "ROL") VALUES (3, 362, 3);

create table permisos
(
	id int not null,
	nombre varchar2(50)
)

comment on table permisos is 'Listado de permisos que puede tener un rol'

comment on column permisos.nombre is 'Nombre del permiso'

create unique index permisos_id_uindex
	on permisos (id)

alter table permisos
	add constraint permisos_pk
		primary key (id);
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (1, 'usuarios:agregar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (2, 'usuarios:editar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (3, 'usuarios:eliminar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (4, 'usuarios:ver');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (5, 'grafica:nacimiento:ver');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (6, 'grafica:registro:ver');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (7, 'permisos:agregar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (8, 'permisos:editar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (9, 'permisos:eliminar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (10, 'permisos:ver');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (11, 'roles:agregar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (12, 'roles:editar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (13, 'roles:eliminar');
INSERT INTO "HR"."PERMISOS" ("ID", "NOMBRE") VALUES (14, 'roles:ver');

INSERT INTO rol_permiso VALUES(1,(SELECT id FROM permisos WHERE nombre = 'usuarios:agregar'), 'admin');
INSERT INTO rol_permiso VALUES(2,(SELECT id FROM permisos WHERE nombre = 'usuarios:editar'), 'admin');
INSERT INTO rol_permiso VALUES(3,(SELECT id FROM permisos WHERE nombre = 'usuarios:eliminar'), 'admin');
INSERT INTO rol_permiso VALUES(4,(SELECT id FROM permisos WHERE nombre = 'usuarios:ver'), 'admin');
INSERT INTO rol_permiso VALUES(5,(SELECT id FROM permisos WHERE nombre = 'grafica:nacimiento:ver'), 'admin');
INSERT INTO rol_permiso VALUES(6,(SELECT id FROM permisos WHERE nombre = 'grafica:registro:ver'), 'admin');
INSERT INTO rol_permiso VALUES(7,(SELECT id FROM permisos WHERE nombre = 'usuarios:ver'), 'dba');
INSERT INTO rol_permiso VALUES(8,(SELECT id FROM permisos WHERE nombre = 'grafica:nacimiento:ver'), 'dba');
INSERT INTO rol_permiso VALUES(9,(SELECT id FROM permisos WHERE nombre = 'grafica:registro:ver'), 'dba');
INSERT INTO rol_permiso VALUES(10,(SELECT id FROM permisos WHERE nombre = 'usuarios:editar'), 'dba');
INSERT INTO rol_permiso VALUES(11,(SELECT id FROM permisos WHERE nombre = 'usuarios:ver'), 'user');

alter table ROL_PERMISO modify PERMISO int

alter table ROL_PERMISO
	add constraint ROL_PERMISO_PERMISOS_ID_fk
		foreign key (PERMISO) references PERMISOS;
comment on column ROL_PERMISO.ROL is 'Contiene el identificador del rol del usuario'

alter table ROL_PERMISO modify ROL number

create unique index ROL_PERMISO_ROL_uindex
	on ROL_PERMISO (ROL)

alter table ROL_PERMISO drop constraint ROL_PERMISO_PERMISOS_ID_FK

alter table ROL_PERMISO
	add constraint ROL_PERMISO_PERMISOS_ID_FK
		foreign key (PERMISO) references PERMISOS

alter table ROL_PERMISO
	add constraint ROL_PERMISO_ROLES_ID_fk
		foreign key (ROL) references ROLES