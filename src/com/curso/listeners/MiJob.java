package com.curso.listeners;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.curso.model.formBeans.dao.UsuarioFormDao;
import com.curso.source.AttachableFile;
import com.curso.source.SendMail;

public class MiJob implements org.quartz.Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date()));
		try {
			sendMail();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendMail() throws IOException {

		//String remitentes = "froy.villaverde@gmail.com";
		String remitentes = UsuarioFormDao.getCorreosDeUsuarios();
		System.out.println("Se enviar� correos a: "+remitentes);
		Map<String, AttachableFile> map = new HashMap<String, AttachableFile>();
		List<Byte> byteList = new ArrayList<Byte>();
		FileInputStream fis = new FileInputStream(
				"C:\\Users\\Froy\\workspace_EE2\\AjaxQueryStrutsHibernateTest\\WebContent\\images\\descarga1.png");
		byte[] b = new byte[1024];

		while (fis.read(b) != -1) {
			for (int i = 0; i < 1024; i++) {
				byteList.add(b[i]);
			}
		}
		byte[] data = new byte[byteList.size()];
		for (int i = 0; i < data.length; i++) {
			data[i] = byteList.get(i);
		}
		AttachableFile temporal = new AttachableFile("descarga1.png", "image/png", data);
		map.put("descarga1.png", temporal);
		fis.close();

		SendMail.execute(
				"<h1 style=\"color:#0033cc;\"> Env�o de correo por medio de cron </h1>"
						+ "</p>Aqui va otro texto de prueba shalala shalala...</p>",
				remitentes, "Prueba env�o de correo con cron", map); 

		
	}

}
