package com.curso.listeners;




import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
/**
 * Un cron es un tipo de hilo, 
 * @author Froy
 *
 */
public class CronEnvioCorreo {
//	private final String cronExpresion = "59 "+new SimpleDateFormat("mm").format(new Date())+" 12 1/1 * ? *";//Expresión que indica el tiempo que se ejecutará el cron
	int minuto = Integer.parseInt(new SimpleDateFormat("mm").format(new Date()));
	int hora = Integer.parseInt(new SimpleDateFormat("HH").format(new Date()));
	private final String cronExpresion = "59 "+ minuto +" "+hora+" 1/1 * ? *";//Expresión que indica el tiempo que se ejecutará el cron
	
	Scheduler scheduler = null;
	
//	public static void main(String[] args) {//Para pruebas de cron en Standalone
//		new CronEnvioCorreo();
//	}

	{
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
		} catch (SchedulerException se) {se.printStackTrace();} 
	}

	public CronEnvioCorreo() {
		init();
	}
	public void init() {
		crearJob();
	}
	
	public void crearJob(){
		//
		JobDetail job = JobBuilder.newJob(MiJob.class).withIdentity("MiCron", "MiCronGroup").build();//Se construye el Job de la clase MiJob
		Trigger trigger = TriggerBuilder.newTrigger()
				.withIdentity("MiCronTrigger", "MiCronGroupTrigger")
				.withSchedule(CronScheduleBuilder.cronSchedule(cronExpresion)).build();//Se construye el lanzador del job
		try {
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		} 
	}
}
