package com.curso.source;


import com.gs.mailManager.core.vo.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailSenderEBS {
	  private Properties properties;
	  private String from;
	  private String subject;
	  private String content;
	  private List<BodyPart> attachments;
	  private List<String> recipients;

	  public Response send()
	  {
		  
	    Session session = null;
	    Multipart multipart = null;
	    BodyPart messageBodyPart = null;
	    Response respuesta = new Response();

	    if (this.properties == null)
	    {
	      respuesta.setStatus(7);
	      respuesta.setMsjStatus("[ERROR] Correo no enviado, consulte los detalles para mayor informacion");
	      respuesta.setDetail("El atributo properties no puede ser null");
	      return respuesta;
	    }

	    if (!this.properties.containsKey("mail.smtp.host"))
	    {
	      respuesta.setStatus(8);
	      respuesta.setMsjStatus("[ERROR] Correo no enviado, consulte los detalles para mayor informacion");
	      respuesta.setDetail("No se ha encontrado la clave 'mail.smtp.host' en el atributo priperties especificado");
	      return respuesta;
	    }

	    if ((this.recipients == null) || (this.recipients.isEmpty())) {
	      respuesta.setStatus(3);
	      respuesta.setMsjStatus("[ERROR] Correo no enviado, consulte los detalles para mayor informacion");
	      respuesta.setDetail("Por lo menos debe especificar una direccion de destino");
	      return respuesta;
	    }

	    if (this.from == null)
	    {
	      respuesta.setStatus(9);
	      respuesta.setMsjStatus("[ERROR] Correo no enviado, consulte los detalles para mayor informacion");
	      respuesta.setDetail("Debe especificar una direccion origen (from)");
	      return respuesta;
	    }

	    if ((this.content == null) || (this.content.length() == 0))
	    {
	      respuesta.setStatus(10);
	      respuesta.setMsjStatus("[ERROR] Correo no enviado, consulte los detalles para mayor informacion");
	      respuesta.setDetail("No puede enviar un correo vacio, por favor agregue contenido");
	      return respuesta;
	    }

	    if ((this.subject == null) || (this.subject.length() == 0))
	    {
	      this.subject = "Sin asunto";
	    }

	    InternetAddress mailAddress = testMail(this.from);
	    if (mailAddress == null) {
	      respuesta.setStatus(5);
	      respuesta.setMsjStatus("[ERROR] Correo no enviado, consulte los detalles para mayor informacion");
	      respuesta.setDetail("La direccion especificada en la propiedad 'from' no es valida");
	      return respuesta;
	    }

	    
	    InternetAddress inetAddressFrom = mailAddress;

	    respuesta.setStatus(0);
	    respuesta.setMsjStatus("[SUCCESS] Mensaje enviado");
	    respuesta.setMsjStatus("�El mensaje se ha enviaado exitosamente a todos los destinatarios!");
	    try {
			final String username=(String) this.properties.get("mail.smtp.user");
			final String password=(String) this.properties.get("mail.smtp.password");
	      session = Session.getDefaultInstance(this.properties, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
	      MimeMessage message = new MimeMessage(session);
	      message.setFrom(inetAddressFrom); 
	      message.setSubject(this.subject);

	      multipart = new MimeMultipart();
	      messageBodyPart = new MimeBodyPart();
	      messageBodyPart.setHeader("Content-Type", "text/html; charset=iso-8859-1");
	      messageBodyPart.setContent(this.content, "text/html; charset=iso-8859-1");
	      multipart.addBodyPart(messageBodyPart);

	      if(attachments != null){
		      System.out.println("attachments.size(): "+attachments.size());
	    	  for(BodyPart bodyPart: attachments){
	    		  multipart.addBodyPart(bodyPart);
	    	  }
	      }
	      message.setHeader("Content-Type", "multipart/mixed");
	      message.setContent(multipart);
	      System.out.println("SendMail.execute()4");
	      for (String address : this.recipients)
	        try {
	          message.setRecipient(Message.RecipientType.BCC, new InternetAddress(address));
	          System.out.println("MailSenderEBS.send()");
	          Transport.send(message);
	          
	          respuesta.getValidEMails().add(address);
	        } catch (MessagingException messagingException) {
	          System.out.println(messagingException.getLocalizedMessage());
	          respuesta.setStatus(1);
	          respuesta.setMsjStatus("[WARNING] Correo enviado parcialmente, consulte los detalles para mayor información");
	          respuesta.setDetail("No se pudo enviar el correo a todas la direcciones especificadas,  debido a que algunas direcciones de correo no se encontrarón en el servidor " +
	            this.properties.getProperty("mail.smtp.host") +
	            ". Para consultar la lista de correos enviados y correos no enviados utilize el método " +
	            "getValidEMails() y getNotValidEMails de la clase Response");
	          respuesta.getInvalidEMails().add(address);
	        }
	    } catch (MessagingException messagingException) {
	      respuesta.setStatus(2);
	      respuesta.setMsjStatus("[ERROR] Correo no enviado, consulte los detalles para mayor información");
	      respuesta.setDetail(messagingException.getLocalizedMessage());
	    } finally {
	      if (respuesta.getValidEMails().isEmpty())
	      {
	        respuesta.setStatus(6);
	        respuesta.setMsjStatus("[ERROR] Correo no enviado, consulte los detalles para mayor información");
	        respuesta.setDetail("Ninguna de las direcciones de correo especificadas es valida en el servidor " +
	          this.properties.getProperty("mail.smtp.host") + ". Verfique que el servidor sea el correcto");
	      }
	    }
	    return respuesta;
	  }

	  private InternetAddress testMail(String mail) {
	    try {
	      return new InternetAddress(mail); } catch (AddressException addressException) {
	    }
	    return null;
	  }

	  public String getContent()
	  {
	    return this.content;
	  }

	  public MailSenderEBS content(String content) {
	    this.content = content;
	    return this;
	  }

	  public String getFrom() {
	    return this.from;
	  }

	  public MailSenderEBS from(String from) {
	    this.from = from;
	    return this;
	  }

	  public Properties getProperties() {
	    return this.properties;
	  }

	  public MailSenderEBS properties(Properties properties) {
	    this.properties = properties;
	    return this;
	  }

	  public List<String> getRecipients() {
	    return this.recipients;
	  }

	  public MailSenderEBS recipients(List<String> recipients) {
	    this.recipients = recipients;
	    return this;
	  }

	  public String getSubject() {
	    return this.subject;
	  }

	  public MailSenderEBS subject(String subject) {
	    this.subject = subject;
	    return this;
	  }

	public List<BodyPart> getAttachments() {
		return attachments;
	}

	public void addAttachments(BodyPart attachments) {
		if(this.attachments == null)
			this.attachments=new ArrayList<BodyPart> ();
		this.attachments.add(attachments);
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

}
