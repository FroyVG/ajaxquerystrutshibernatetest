package com.curso.source;

import java.util.Arrays;

public class AttachableFile {
	private String name;
	private String mime;
	private byte[] data;

	public AttachableFile() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AttachableFile(String name, String mime, byte[] data) {
		super();
		this.name = name;
		this.mime = mime;
		this.data = data;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMime() {
		return mime;
	}
	public void setMime(String mime) {
		this.mime = mime;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "AttachableFile [name=" + name + ", mime=" + mime + ", data="
				+ Arrays.toString(data) + "]";
	}
}
