package com.curso.source;

import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

import com.gs.mailManager.core.MailManager;
import com.gs.mailManager.core.vo.Response;

import com.curso.source.AttachableFile;

public class SendMail {
//	public static void main(String[] args) {
//		String reemitentes="euriel.baez@gmail.com";
//		
//		SendMail.execute("probando body", reemitentes, "asunto de probando", null);
//	}

	public static void execute(String mensaje, String reemitentes, String asunto, Map<String, AttachableFile> mapNombreData)	  {
		//final String username = "java.test.mail001@gmail.com";
		//final String password = "test.mail001";
		final String username = "mailjava90@gmail.com";
		final String password = "JavaMail_123";
		
		Properties props = new Properties();
		props.put("mail.smtp.user", username);
		props.put("mail.smtp.password", password);
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.debug", "true");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.starttls.enable", "true");
	    props.put("mail.smtp.EnableSSL.enable", "true");
	    
	    props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    props.setProperty("mail.smtp.port", "465");
	    props.setProperty("mail.smtp.socketFactory.port", "465");
	    props.put("monitoreo.addressList", "java.test.mail001@gmail.com");
	    props.put("mailManager.addressList.tokenSeparator", "[,]");
	    
	    MailManager mm = new MailManager(props);
	    mm.setRecipients(mm.getMailList(reemitentes));
	    Response respuesta = null;
	    StringBuilder mensajeInterno = new StringBuilder(mensaje);
	    StringBuilder asuntoInterno = new StringBuilder(asunto);


	    mm.setFrom("Java Mail <mailjava90@gmail.com>");

//	    mm.setFrom("EjecutaQueryJavaX <BWREMOTE@correo.mex.gigante>");
	    mm.setSubject(asuntoInterno.toString());
	    mm.setHtmlMensaje(mensajeInterno.toString());
		mm.getProperties().put("mail.smtp.host", "smtp.gmail.com");
		mm.getProperties().put("mail.smtp.port", "587");

		mm.getProperties().put("mail.smtp.user", username);
		mm.getProperties().put("mail.smtp.password", password);
	    BodyPart adjunto = null;
	    MailSenderEBS senderEBS = new MailSenderEBS();
	    
	    try {
	    	if(mapNombreData!=null)
	    		for(String nombreArchivo: mapNombreData.keySet()){
	    			adjunto = new MimeBodyPart();
	    			adjunto.setDataHandler(
	    					new DataHandler(
	    							(DataSource) new ByteArrayDataSource(mapNombreData.get(nombreArchivo).getData(),mapNombreData.get(nombreArchivo).getMime())));
	    			adjunto.setFileName(nombreArchivo);
	    			senderEBS.addAttachments(adjunto);
	    		}
		} catch (MessagingException e) {			e.printStackTrace();		}

	    
	    respuesta = senderEBS.content(mm.getHtmlMensaje())
	      .from(mm.getFrom())
	      .recipients(mm.getRecipients())
	      .subject(mm.getSubject())
	      .properties(mm.getProperties())
	      .send();



	    if(respuesta.getInvalidEMails().size()>0){
	    	System.out.println("Status: " + respuesta.getStatus());
	    	System.out.println("MsjStatus: " + respuesta.getMsjStatus());
	    	System.out.println("detail: " + respuesta.getDetail());
	    	System.out.println("ValidEMails: " + respuesta.getValidEMails());
	    	System.out.println("invalidEMails: " + respuesta.getInvalidEMails());
	    }
	  }
}
