package com.curso.model.graficoPie;

import java.util.ArrayList;
import java.util.List;

public class GraficaPie {
	private List<Slice> data;
	private String categoria;
	
	public GraficaPie() {
		
	}

	public GraficaPie(List<Slice> data, String categoria) {
		super();
		this.data = data;
		this.categoria = categoria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GraficaPie other = (GraficaPie) obj;
		if (categoria == null) {
			if (other.categoria != null)
				return false;
		} else if (!categoria.equals(other.categoria))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	public List<Slice> getData() {
		return data;
	}

	public void setData(List<Slice> data) {
		this.data = data;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@Override
	public String toString() {
		return "GraficaPie [data=" + data + ", categoria=" + categoria + "]";
	}

	
}
