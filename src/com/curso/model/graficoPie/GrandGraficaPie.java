package com.curso.model.graficoPie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.curso.model.formBeans.UsuarioForm;
import com.curso.util.DateFormatter;

public class GrandGraficaPie {
	private List<String> categorias = new ArrayList<String>();
	private List<GraficaPie> graficasPie = new ArrayList<GraficaPie>();

	public GrandGraficaPie(List<UsuarioForm> usuarios) {
		// Obtenemos las categorias que son los a�os
		Set<String> categoriasSet = new HashSet<String>();
		for (UsuarioForm usuario : usuarios) {
			categoriasSet.add(DateFormatter.justYearToString(usuario.getFechaNacimiento()));
		}
		CollectionUtils.addAll(categorias, categoriasSet.iterator());
		Collections.sort(categorias);

		// Para cada categor�a agregamos una nueva grafica con el a�o que le corresponde
		for (String anio : categorias) {
			graficasPie.add(new GraficaPie(new ArrayList<Slice>(), anio));
		}
		
		//Agregamos a cada usuario en la grafica que le corresponde
		for (UsuarioForm usuario : usuarios) {
			for (GraficaPie graficaPie  : graficasPie) {//Recorremos todos los a�os
				if (DateFormatter.justYearToString(usuario.getFechaNacimiento()).equals(graficaPie.getCategoria()) ) {
					graficaPie.getData().add(new Slice(usuario.getNombre(), 1));
				}
			}
		}

	}

	public List<String> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<String> categorias) {
		this.categorias = categorias;
	}

	public List<GraficaPie> getGraficasPie() {
		return graficasPie;
	}

	public void setGraficasPie(List<GraficaPie> graficasPie) {
		this.graficasPie = graficasPie;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categorias == null) ? 0 : categorias.hashCode());
		result = prime * result + ((graficasPie == null) ? 0 : graficasPie.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrandGraficaPie other = (GrandGraficaPie) obj;
		if (categorias == null) {
			if (other.categorias != null)
				return false;
		} else if (!categorias.equals(other.categorias))
			return false;
		if (graficasPie == null) {
			if (other.graficasPie != null)
				return false;
		} else if (!graficasPie.equals(other.graficasPie))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GrandGraficaPie [categorias=" + categorias + ", graficasPie=" + graficasPie + "]";
	}

}
