package com.curso.model.formBeans;

import java.io.Serializable;

public class Rol implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3404070929750303144L;
	private int id;
	private String nombre;
	
	public Rol() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Rol [id=" + id + ", nombre=" + nombre + "]";
	}
	
}
