package com.curso.model.formBeans.dao;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.Rol;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class RolDao {
	private boolean statusOk;
	private String mensaje;

	public RolDao() {
		statusOk = true;
		mensaje = "";
	}

	public static List<Rol> readRolByPerson(String personId)
			throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {

		Client client = Client.create();
		WebResource resource = client.resource("http://localhost:8083/RestfulApi/rest/leerRolByPersona");
		ClientResponse response = resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, personId);//Aqu� modifiqu� REVISAR
		String rolesJson = response.getEntity(String.class);
		ObjectMapper om = new ObjectMapper();
		List<Rol> roles;
		roles = om.convertValue(om.readValue(rolesJson, JsonNode.class), new TypeReference<List<Rol>>() { });
		return roles;

	}

	public static List<Rol> selectAllRoles()
			throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {

		Client cliente = Client.create();
		WebResource resource = cliente.resource("http://localhost:8083/RestfulApi/rest/leerTodosLosRoles");
		String rolesJson = resource.type(MediaType.APPLICATION_JSON).get(String.class);
		ObjectMapper om = new ObjectMapper();
		List<Rol> roles;
		roles = om.convertValue(om.readValue(rolesJson, JsonNode.class), new TypeReference<List<Rol>>() { });
		return roles;
	}
	
	public static boolean insertaRol(Rol rol) {
		Client client = Client.create();
		boolean resultado = false;
		try {
			WebResource webResource = client.resource("http://localhost:8083/RestfulApi/rest/insertaRol");
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, rol);// JSON:
																												// "{\'id\':\'278\'}"
			String permisoResponse = response.getEntity(String.class);
			ObjectMapper om = new ObjectMapper();
			Rol newRol = om.readValue(permisoResponse, Rol.class);
			rol.setId(newRol.getId());
			resultado = true;
		} catch (Exception e) {
			e.printStackTrace();
			resultado = false;
		}
		return resultado;
	}
	
	public static Rol eliminaRolById(int id) throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {
		Client cliente = Client.create();
		WebResource webResource = cliente.resource("http://localhost:8083/RestfulApi/rest/eliminaRol");
		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, id+"");
		String usuario = response.getEntity(String.class);
		ObjectMapper om = new ObjectMapper();
		Rol rol = om.readValue(usuario, Rol.class);
		return rol;
	}
	
	public static boolean editarRol(Rol uf) {
		boolean resultado=false;
		Client cliente = Client.create();
		try {
			WebResource resource = cliente.resource("http://localhost:8083/RestfulApi/rest/editaRol");
			ClientResponse response = (ClientResponse) resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, uf);
			response.getEntity(String.class);
			resultado = true;
		} catch (Exception e1) {
			resultado = false;
		}
		return resultado;
	}

	public static Rol readRolByNombre(String rolName)
			throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {

		Client client = Client.create();
		WebResource resource = client.resource("http://localhost:8083/RestfulApi/rest/leerRolByNombre");
		ClientResponse response = resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, rolName);
		String rolJson = response.getEntity(String.class);
		ObjectMapper om = new ObjectMapper();
		Rol rol;
		rol = om.convertValue(om.readValue(rolJson, JsonNode.class), Rol.class);
		return rol;

	}
	public boolean isStatusOk() {
		return statusOk;
	}

	public void setStatusOk(boolean statusOk) {
		this.statusOk = statusOk;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
