package com.curso.model.formBeans.dao;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.Rol;
import com.curso.model.formBeans.RolPermiso;
import com.curso.model.formBeans.UsuarioForm;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class PermisoDao {

	private boolean statusOK;
	private String mensaje;

	public PermisoDao() {
		statusOK = true;
		mensaje = "";
	}

	public static List<Permiso> readPermisosByRol(String rolName)
			throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {

		Client cliente = Client.create();
		WebResource resource = cliente.resource("http://localhost:8083/RestfulApi/rest/leerPermisosByRol");
		ClientResponse response = resource.type("application/json").post(ClientResponse.class, rolName);
		String permisos = response.getEntity(String.class);
		ObjectMapper om = new ObjectMapper();
		List<Permiso> permisosList;
		permisosList = om.convertValue(om.readValue(permisos, JsonNode.class), new TypeReference<List<Permiso>>() {
		});
		return permisosList;

	}

	public static List<Permiso> readAllPermisos()
			throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {

		Client cliente = Client.create();
		WebResource resource = cliente.resource("http://localhost:8083/RestfulApi/rest/leerTodosLosPermisos");
		String permisosJson = resource.type(MediaType.APPLICATION_JSON).get(String.class);
		ObjectMapper om = new ObjectMapper();
		List<Permiso> permisos;
		permisos = om.convertValue(om.readValue(permisosJson, JsonNode.class), new TypeReference<List<Permiso>>() {
		});
		return permisos;
	}

	public static boolean insertaPermiso(Permiso permiso) {
		Client client = Client.create();
		boolean resultado = false;
		try {
			WebResource webResource = client.resource("http://localhost:8083/RestfulApi/rest/insertaPermiso");
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, permiso);// JSON:
																												// "{\'id\':\'278\'}"
			String permisoResponse = response.getEntity(String.class);
			ObjectMapper om = new ObjectMapper();
			Permiso newPermiso = om.readValue(permisoResponse, Permiso.class);
			permiso.setId(newPermiso.getId());
			resultado = true;
		} catch (Exception e) {
			e.printStackTrace();
			resultado = false;
		}
		return resultado;
	}

	public static Permiso eliminaPermisoById(int id)
			throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {
		Client cliente = Client.create();
		WebResource webResource = cliente.resource("http://localhost:8083/RestfulApi/rest/eliminaPermiso");
		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, id + "");
		String usuario = response.getEntity(String.class);
		ObjectMapper om = new ObjectMapper();
		Permiso permiso = om.readValue(usuario, Permiso.class);
		return permiso;
	}

	public static boolean editarPermiso(Permiso uf) {
		boolean resultado = false;
		Client cliente = Client.create();
		try {
			WebResource resource = cliente.resource("http://localhost:8083/RestfulApi/rest/editaPermiso");
			ClientResponse response = (ClientResponse) resource.type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class, uf);
			response.getEntity(String.class);
			resultado = true;
		} catch (Exception e1) {
			resultado = false;
		}
		return resultado;
	}

	public static boolean insertaPermisoRol(List<RolPermiso> rolPermiso) {
		Client client = Client.create();
		boolean resultado = false;
		try {
			WebResource webResource = client.resource("http://localhost:8083/RestfulApi/rest/insertaPermisoByRol");
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, rolPermiso);// JSON: "{\'id\':\'278\'}"
			response.getEntity(String.class);
			resultado = true;
		} catch (Exception e) {
			e.printStackTrace();
			resultado = false;
		}
		return resultado;
	}

	public static boolean eliminaPermisoRol(List<RolPermiso> rolPermisosListRemove) {
		Client client = Client.create();
		boolean resultado;
		try {
			System.out.println("removePermisos="+rolPermisosListRemove);
			WebResource resource = client.resource("http://localhost:8083/RestfulApi/rest/eliminaPermisoByRol");
			ClientResponse response = (ClientResponse) resource.type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class, rolPermisosListRemove);
			response.getEntity(String.class);
			resultado = true;
		} catch (Exception e1) {
			resultado = false;
		}
		return resultado;
	}
	
	public boolean isStatusOK() {
		return statusOK;
	}

	public void setStatusOK(boolean statusOK) {
		this.statusOK = statusOK;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
