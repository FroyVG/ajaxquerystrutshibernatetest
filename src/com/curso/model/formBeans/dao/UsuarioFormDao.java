package com.curso.model.formBeans.dao;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.curso.model.formBeans.UsuarioForm;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.RequestBuilder;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;

/*********
 * Clase TransactionEntryDAO administra bajo el patron DAO increso a DB por
 * modelo CRUD
 **********/
public class UsuarioFormDao {
//********** Variables miembro **********
	private boolean statusOK;
	private String mensaje;
//---------- Variables miembro ----------

//********** Constructores **********
	public UsuarioFormDao() {
		statusOK = true;
		mensaje = "";
	}
//---------- Constructores ----------

//********** Metodos CRUD **********
	/**
	 * Se implementa m�todo para leer todos los usuarios a trav�s de un servicio web
	 * 
	 * @return
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public static List<UsuarioForm> readTodosUsuarios() throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {
		
		Client cliente = Client.create();
		WebResource resource = cliente.resource("http://localhost:8083/RestfulApi/rest/leerTodosLosUsuarios");
		String usuarios = resource.type(MediaType.APPLICATION_JSON).get(String.class);
		ObjectMapper om = new ObjectMapper();
		List<UsuarioForm> temporal;
			temporal = om.convertValue(om.readValue(usuarios, JsonNode.class) , new TypeReference<List<UsuarioForm>>(){});
		return temporal;
		
		/*
		 * resource = cliente.resource("http://localhost:8080/Restful/rest/insertarUsuario");
		 * Usuario temporal2 = new Usuario("Elena", "seniora2", 15); 
		 * ClientResponse response = (ClientResponse) resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, temporal2); 
		 * System.out.println(response.getEntity(String.class));
		 */
	}

	/**
	 * M�todo que elimina un usuario.
	 * 
	 * @return UsuarioForm del objeto eliminado
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */

	public static UsuarioForm eliminaUsuarioById(int id) throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {
		Client cliente = Client.create();
		WebResource webResource = cliente.resource("http://localhost:8083/RestfulApi/rest/eliminaUsuario");
		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, id+"");
		String usuario = response.getEntity(String.class);
		ObjectMapper om = new ObjectMapper();
		UsuarioForm usuarioForm = om.readValue(usuario, UsuarioForm.class);
		return usuarioForm;
	}

	/**
	 * Se implementa m�todo para guardar un usuario a trav�s de un servicio web.
	 * 
	 * @return
	 */
	public static synchronized boolean guardarUsuario(UsuarioForm ptt) {
		boolean resultado=false;
		Client cliente = Client.create();
		try {
			WebResource resource = cliente.resource("http://localhost:8083/RestfulApi/rest/insertaUsuario");
			ClientResponse response = (ClientResponse) resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, ptt);
			String usuario = response.getEntity(String.class);
			ObjectMapper om = new ObjectMapper();
			UsuarioForm usuarioForm = om.readValue(usuario, UsuarioForm.class);
			ptt.setId(usuarioForm.getId());
			resultado = true;
		} catch (Exception e1) {
			resultado = false;
			e1.printStackTrace();
		}
		return resultado;
	}

	/**
	 * Se implementa m�todo para editar un usuario a trav�s de un servicio web.
	 * 
	 * @return
	 */
	public static boolean editarUsuario(UsuarioForm uf) {
		boolean resultado=false;
		Client cliente = Client.create();
		try {
			WebResource resource = cliente.resource("http://localhost:8083/RestfulApi/rest/editaUsuario");
			ClientResponse response = (ClientResponse) resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, uf);
			response.getEntity(String.class);
			resultado = true;
		} catch (Exception e1) {
			resultado = false;
		}
		return resultado;
	}

	public static String getCorreosDeUsuarios() {
		List<UsuarioForm> usuarioForms = null;
		try {
			usuarioForms = readTodosUsuarios();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String correos="";
		for (UsuarioForm usuarioForm : usuarioForms) {
			correos = correos.concat( usuarioForm.getCorreo());
			correos = correos.concat(",");
		}
		System.out.println("correos: "+correos.substring(0, (correos.length()>0)?correos.length()-1:0));
		return correos.substring(0, (correos.length()>0)?correos.length()-1:0);
	}
	
	public static UsuarioForm getUserByMail(String mail) throws JsonParseException, JsonMappingException, IllegalArgumentException, IOException {
		return readTodosUsuarios().stream().filter(p -> p.getCorreo().equals(mail)).findFirst().get();
	}
//--------- Metodo readListaConsultasEspecificas permite consultar una lista de consultas por su nombre ----------/	
//---------- Metodos CRUD ----------

//********** Metodos GETTERS Y SETTERS **********
	public boolean isStatusOK() {
		return statusOK;
	}

	public void setStatusOK(boolean statusOK) {
		this.statusOK = statusOK;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
//---------- Metodos GETTERS Y SETTERS ----------

}