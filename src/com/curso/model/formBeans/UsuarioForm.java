package com.curso.model.formBeans;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsuarioForm implements Serializable {
	
	private static final long serialVersionUID = 2252989333052624316L;
	private Integer id;
	private String nombre;
	private String apellido;
	private Integer edad;
	private String correo;
	private Date fechaNacimiento;
	private Date fechaRegistro;
	private SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

	public UsuarioForm() {
	}
	
	public UsuarioForm(Integer id, String nombre, String apellido, Integer edad, String correo, Date fechaNacimiento,
			Date fechaRegistro) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.fechaNacimiento = fechaNacimiento;
		this.edad = edad;
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	@Override
	public String toString() {
		return "UsuarioForm [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad
				+ ", correo=" + correo + ", fechaNacimiento=" + sdf.format(fechaNacimiento ) + ", fechaRegistro=" + sdf.format(fechaRegistro) 
				+ "]";
	}

}
