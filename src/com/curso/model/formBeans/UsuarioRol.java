package com.curso.model.formBeans;

public class UsuarioRol {
	
	private int id;
	private int usuarioId;
	private int rolId;
	
	public UsuarioRol() {
	}
	public UsuarioRol(int id, int usuarioId, int rolId) {
		super();
		this.id = id;
		this.usuarioId = usuarioId;
		this.rolId = rolId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	public int getRolId() {
		return rolId;
	}
	public void setRolId(int rolId) {
		this.rolId = rolId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + rolId;
		result = prime * result + usuarioId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioRol other = (UsuarioRol) obj;
		if (id != other.id)
			return false;
		if (rolId != other.rolId)
			return false;
		if (usuarioId != other.usuarioId)
			return false;
		return true;
	}

}
