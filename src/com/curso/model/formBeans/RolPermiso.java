package com.curso.model.formBeans;

public class RolPermiso {

	private int id;
	private int permisoId;
	private int rolId;
	
	public RolPermiso() {
	}
	public RolPermiso(int id, int permisoId, int rolId) {
		this.id = id;
		this.permisoId = permisoId;
		this.rolId = rolId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPermisoId() {
		return permisoId;
	}
	public void setPermisoId(int permisoId) {
		this.permisoId = permisoId;
	}
	public int getRolId() {
		return rolId;
	}
	public void setRolId(int rolId) {
		this.rolId = rolId;
	}
	@Override
	public String toString() {
		return "RolPermiso [id=" + id + ", permisoId=" + permisoId + ", rolId=" + rolId + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + permisoId;
		result = prime * result + rolId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolPermiso other = (RolPermiso) obj;
		if (id != other.id)
			return false;
		if (permisoId != other.permisoId)
			return false;
		if (rolId != other.rolId)
			return false;
		return true;
	}

}
