package com.curso.model.graficoBarras;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.curso.model.formBeans.UsuarioForm;
import com.curso.util.DateFormatter;

public class GraficaBarras {
	private List<Serie> series = new ArrayList<Serie>();
	private List<String> categorias = new ArrayList<String>();
	
	public GraficaBarras(List<UsuarioForm> usuarios) {
		Set<String> categoriasSet = new HashSet<String>();
		for (UsuarioForm usuario : usuarios) { 
			categoriasSet.add(DateFormatter.justYearToString( usuario.getFechaNacimiento()));
		}
		CollectionUtils.addAll(categorias, categoriasSet.iterator());
		Collections.sort(categorias);
		
		for (UsuarioForm usuario : usuarios) {
			series.add( new Serie(usuario.getNombre(), new int [categorias.size()]) );
			series.get(series.size()-1).getData()[categorias.indexOf(DateFormatter.justYearToString( usuario.getFechaNacimiento()))]=1;
		}
		
	}
	
	public GraficaBarras(List<Serie> series, List<String> categorias) {
		this.series = series;
		this.categorias = categorias;
	}
	public GraficaBarras() {
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categorias == null) ? 0 : categorias.hashCode());
		result = prime * result + ((series == null) ? 0 : series.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GraficaBarras other = (GraficaBarras) obj;
		if (categorias == null) {
			if (other.categorias != null)
				return false;
		} else if (!categorias.equals(other.categorias))
			return false;
		if (series == null) {
			if (other.series != null)
				return false;
		} else if (!series.equals(other.series))
			return false;
		return true;
	}
	public List<Serie> getSeries() {
		return series;
	}
	public void setSeries(List<Serie> series) {
		this.series = series;
	}
	public List<String> getCategorias() {
		return categorias;
	}
	public void setCategorias(List<String> categorias) {
		this.categorias = categorias;
	}
	@Override
	public String toString() {
		return "GraficaBarras [series=" + series + ", categorias=" + categorias + "]";
	}
}
