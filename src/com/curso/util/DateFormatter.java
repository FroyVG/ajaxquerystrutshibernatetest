package com.curso.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {
	private static final SimpleDateFormat justYear = new SimpleDateFormat("yyyy");
	
	public static int justYearToInt(Date date){
		return Integer.parseInt( justYear.format(date) );
	}
	public static String justYearToString(Date date){
		return justYear.format(date) ;
	}
	public static String parse(Date date, String pattern){
		return new SimpleDateFormat(pattern).format(date);
	}

}
