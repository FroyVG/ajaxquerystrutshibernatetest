package com.curso.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubjectUserInfo {
	private boolean status;
	private String mensaje;
	private static final Logger LOG = LoggerFactory.getLogger(SubjectUserInfo.class);

	public SubjectUserInfo() {
	}

	public SubjectUserInfo(boolean status, String mensaje) {
		super();
		this.status = status;
		this.mensaje = mensaje;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public static Subject obtenerSubject(HttpServlet servlet) {
		return obtenerSubject(servlet.getServletContext());
		
	}
	public static Subject obtenerSubject(HttpServletRequest request) {
		return obtenerSubject(request.getServletContext());
	}
	
	public static Subject obtenerSubject(ServletContext servletContext) {
		SecurityUtils.setSecurityManager(WebUtils.getRequiredWebEnvironment(servletContext).getSecurityManager());
		return SecurityUtils.getSubject();
	}

	public static SubjectUserInfo loggear(Subject sujeto, String usuario, String password) {

		SubjectUserInfo subjectUserInfo = new SubjectUserInfo();
		Session session = null;
		if (!sujeto.isAuthenticated()) {
			UsernamePasswordToken token = new UsernamePasswordToken(usuario, password);
			token.setRememberMe(true);
			try {
				subjectUserInfo.setStatus(true);
				sujeto.login(token);
				//ThreadContext.bind(sujeto);
				subjectUserInfo.setMensaje("Usuario [" + sujeto.getPrincipal() + "] loggeado exitosamente.");
				session = sujeto.getSession();
				LOG.info("Host: "+session.getHost() + ", id: " + session.getId() + ", timeOut: " + session.getTimeout());
			} catch (AuthenticationException ae) {
				subjectUserInfo.setStatus(false);
				subjectUserInfo.setMensaje("La cuenta para el usuario: " + token.getPrincipal() + " no se pudo autenticar.");
				if (ae.getMessage().indexOf(UnknownAccountException.class.getName()) > 0) {
					subjectUserInfo.setStatus(false);
					subjectUserInfo.setMensaje("No existe el usuario: " + token.getPrincipal());
					LOG.error(UnknownAccountException.class.getName());
					LOG.error("No existe el usuario: " + token.getPrincipal());
				} else if (ae.getMessage().indexOf(IncorrectCredentialsException.class.getName()) > 0) {
					subjectUserInfo.setStatus(false);
					subjectUserInfo.setMensaje("Password para la cuenta: " + token.getPrincipal() + " es incorrecta.");
				} else if (ae.getMessage().indexOf(LockedAccountException.class.getName()) > 0) {
					subjectUserInfo.setStatus(false);
					subjectUserInfo.setMensaje("La cuenta para el usuario: " + token.getPrincipal()
							+ " esta bloqueada, por favor contacta al administrador para desbloquearla.");
				} else if (ae.getMessage().indexOf(UsernamePasswordToken.class.getName()) > 0) {
					subjectUserInfo.setStatus(false);
					subjectUserInfo.setMensaje("La cuenta para el usuario: " + token.getPrincipal()
							+ " no concuerdan usuario o contraseņa.");
				}
			}
		} else {
			subjectUserInfo.setStatus(true);
			subjectUserInfo.setMensaje("Sujeto [" + sujeto.getPrincipal() + "] es el usuario existente.");
		} 
		System.out.println("SubjectUserInfo.loggear()");
		System.out.println(ThreadContext.getSubject());
		System.out.println(ThreadContext.getSubject().getPrincipal());
		System.out.println("==============================="); 
		return subjectUserInfo;
	}
}
