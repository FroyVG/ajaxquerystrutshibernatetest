package com.curso.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.Rol;
import com.curso.model.formBeans.UsuarioForm;
import com.curso.model.formBeans.dao.PermisoDao;
import com.curso.model.formBeans.dao.RolDao;
import com.curso.model.formBeans.dao.UsuarioFormDao;

public class Shiro extends HttpServlet{
	private static final long serialVersionUID = -8540027513023417565L;
	private static final Logger LOG = LoggerFactory.getLogger(Shiro.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			ejecutar(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			ejecutar(req, resp);
	}
	
	public void ejecutar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UsuarioForm usuario;
		String accion = req.getParameter("action");
		Subject subject = SubjectUserInfo.obtenerSubject(this);
		HttpSession session = req.getSession();
		RequestDispatcher rd =req.getRequestDispatcher("index.jsp");
		
		if(accion!=null&&accion.equals("logout")&&subject.isAuthenticated()){
			subject.logout();
			
			req.setAttribute("errorMensaje", "El usuario ha cerrado la session correctamente");
			
				rd.forward(req, resp);
			
			return;
		}
		
		session.removeAttribute("errorMensaje");
		String user;
		String password;
		
		user = req.getParameter("mail");
		password = req.getParameter("pass");
		usuario = UsuarioFormDao.getUserByMail(user);
		boolean errorParametros=false;
		
		if(user==null ||user.trim().length()==0){	 		errorParametros=true; session.setAttribute("errorMensaje", "Ingrese usuario "); }
		if(password==null || password.trim().length()==0){ 	errorParametros=true; session.setAttribute("errorMensaje", (session.getAttribute("errorMensaje")==null)?"":session.getAttribute("errorMensaje") + "<br> Ingrese contraseña"); }
		if(errorParametros){rd.forward(req, resp); return;}
		
		SubjectUserInfo info = SubjectUserInfo.loggear(subject, user, password);
		if(info.isStatus()){
			subject.getSession().setAttribute("Usuario", usuario.getNombre() + " " + usuario.getApellido());
			subject.getSession().setAttribute("personId", usuario.getId());
			enviarRespuesta(resp);
		} else {
			session.setAttribute("errorMensaje", info.getMensaje());
			rd.forward(req, resp);
			}	
	}
	
	public void enviarRespuesta(HttpServletResponse resp) throws IOException{
		resp.sendRedirect("CargaInicialAction.do");
	}

}
