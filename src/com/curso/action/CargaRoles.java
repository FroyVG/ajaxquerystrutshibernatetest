package com.curso.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.Rol;
import com.curso.model.formBeans.dao.PermisoDao;
import com.curso.model.formBeans.dao.RolDao;
import com.curso.util.SubjectUserInfo;

public class CargaRoles extends Action {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Subject currentUser = SubjectUserInfo.obtenerSubject(this.getServlet());
		List<Rol> allRoles = new ArrayList<>();
		allRoles = RolDao.selectAllRoles();
		if (currentUser.isPermitted("roles:ver")) {
			System.out.println(allRoles);
			//request.getSession().setAttribute("roles", allRoles);//El conjunto de roles existentes 
			LOG.info("Si cuenta con el permiso para ver todos los roles.");
		}else {
			request.setAttribute("ErrorPermisos", "No cuentas con los suficientes permisos para visualizar los roles.");
			LOG.warn("No cuentas con los suficientes permisos para visualizar los roles.");
		}
		return null;
//		return new ActionForward("/roles.jsp");
		//return mapping.findForward("success");
	}

}
