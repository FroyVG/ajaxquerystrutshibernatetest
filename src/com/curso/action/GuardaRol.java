package com.curso.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.Rol;
import com.curso.model.formBeans.UsuarioForm;
import com.curso.model.formBeans.dao.PermisoDao;
import com.curso.model.formBeans.dao.RolDao;
import com.curso.model.formBeans.dao.UsuarioFormDao;
import com.curso.util.SubjectUserInfo;

public class GuardaRol extends Action {
	final Logger LOG = LoggerFactory.getLogger(GuardaRol.class);

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LOG.info("GuardaRol.execute()");
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
		Subject currentUser = SubjectUserInfo.obtenerSubject(this.getServlet());

		String x = br.readLine();
		x = x.replace("\\", "");
		x = x.substring(1, x.length() - 1);

		JSONObject temporal = (JSONObject) new JSONParser().parse(x);
		ObjectMapper om = new ObjectMapper();
		Rol uf = om.readValue(temporal.toJSONString(), Rol.class);

		if (currentUser.isPermitted("roles:agregar")) {
			if (RolDao.insertaRol(uf)) {
				om.writeValue(response.getOutputStream(), om.convertValue(uf, Rol.class));
			} else {
				om.writeValue(response.getOutputStream(), "Error");
			}
		} else {
			om.writeValue(response.getOutputStream(), "No tienes permiso para guardar un rol");
			LOG.error("No tienes permiso para guardar un rol");
		}
		return null;
	}

}
