package com.curso.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.UsuarioForm;
import com.curso.model.formBeans.dao.UsuarioFormDao;
import com.curso.util.SubjectUserInfo;
@RequiresPermissions("usuarios:eliminar")
public class EliminaUsuario extends Action{
	private static Logger LOG = LoggerFactory.getLogger(EliminaUsuario.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		BufferedReader br=new BufferedReader( new InputStreamReader( request.getInputStream() ) );
		Subject subject = SubjectUserInfo.obtenerSubject(this.getServlet());
		String x = br.readLine();
		x =  x.replace("\\", "");
		x = x.substring(1, x.length()-1);
		
		JSONObject temporal = (JSONObject) new JSONParser().parse(x);
		
		int usuarioId = Integer.parseInt(temporal.get("data")+"");
		UsuarioForm uf = UsuarioFormDao.eliminaUsuarioById(usuarioId);
		ObjectMapper om = new ObjectMapper();
//		om.readValue(content, valueType)
		om.writeValue(response.getOutputStream(), om.convertValue(uf, UsuarioForm.class) );
		System.out.println("EliminaUsuario.execute()");
		System.out.println("--Subject: " + subject);
		System.out.println("--Principal: " + subject.getPrincipal());
		LOG.info("Se elimina el usuario de forma exitosa.");
		return null;
	}

}
