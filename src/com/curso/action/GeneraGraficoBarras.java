package com.curso.action;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import com.curso.model.formBeans.UsuarioForm;
import com.curso.model.formBeans.dao.UsuarioFormDao;
import com.curso.model.graficoBarras.GraficaBarras;

public class GeneraGraficoBarras extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		GraficaBarras graficaBarras= new GraficaBarras(UsuarioFormDao.readTodosUsuarios());
		ObjectMapper om = new ObjectMapper();
		PrintWriter pw = response.getWriter();
		om.writeValue(pw, graficaBarras);
		return null;
	}

	public static void main(String[] args) throws Exception {
		new GeneraGraficoBarras().execute(null, null, null, null);
	}

}
