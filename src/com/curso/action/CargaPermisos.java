package com.curso.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.Rol;
import com.curso.model.formBeans.dao.PermisoDao;
import com.curso.model.formBeans.dao.RolDao;
import com.curso.util.SubjectUserInfo;

public class CargaPermisos extends Action {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Subject currentUser = SubjectUserInfo.obtenerSubject(this.getServlet());
		List<Permiso> allPermisos = new ArrayList<>();
		allPermisos = PermisoDao.readAllPermisos();
		if (currentUser.isPermitted("permisos:ver")) {
			request.getSession().setAttribute("permisos", allPermisos);//El conjunto de roles existentes 
			LOG.info("Si cuentas con el permiso para ver todos los permisos.");
		}else {
			request.setAttribute("ErrorPermisos", "No cuentas con los suficientes permisos para visualizar los permisos.");
			LOG.warn("No cuentas con los suficientes permisos para visualizar los permisos.");
		}
		return new ActionForward("/permisos.jsp");
		//return mapping.findForward("/permisos.jsp");
	}
}
