package com.curso.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.dao.PermisoDao;
import com.curso.util.SubjectUserInfo;

public class ObtienePermisoByRol extends Action{
	
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String x = br.readLine();
		x = x.replace("\\", "");
		x = x.substring(1, x.length() - 1);
		JSONObject temporal = (JSONObject) new JSONParser().parse(x);
		String rolName = (String) temporal.get("data");
		
		Subject currentUser = SubjectUserInfo.obtenerSubject(this.getServlet());

		List<Permiso> permisosByRol = new ArrayList<Permiso>();
		List<Permiso> permisos = new ArrayList<Permiso>();
		
		if (currentUser.isPermitted("permisos:ver")) {
			permisosByRol = PermisoDao.readPermisosByRol(rolName);
			permisos = PermisoDao.readAllPermisos();
			LOG.info("{} cuenta con los permisos para ver los permisos del rol: {} ",currentUser.getPrincipal() , rolName);
			request.getSession().setAttribute("permisos", permisos);
			request.getSession().setAttribute("permisosByRol", permisosByRol);
		}else {
			LOG.warn("{} cuenta con los permisos para ver los permisos del rol: {} ",currentUser.getPrincipal() , rolName);
			request.getSession().setAttribute("error", "No cuentas con los suficientes permisos para ver permisos.");
		}
//		response.setContentType("text/text; charset=utf-8");
//		response.setHeader("cache-control", "no-cache");
//		PrintWriter pw =response.getWriter();
//		pw.append(new ActionForward("/permisosByRol.jsp").toString());
//		pw.flush();
		ActionForward af = new ActionForward("/permisosByRol.jsp");
//		af.setRedirect(false);
//		af.setContextRelative(false);
//		af.freeze();
		return af;
//		return null;
	}
}
