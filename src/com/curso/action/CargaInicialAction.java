package com.curso.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.Rol;
import com.curso.model.formBeans.dao.PermisoDao;
import com.curso.model.formBeans.dao.RolDao;
import com.curso.model.formBeans.dao.UsuarioFormDao;
import com.curso.util.SubjectUserInfo;

public class CargaInicialAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Subject currentUser = SubjectUserInfo.obtenerSubject(this.getServlet());
		List<Rol> allRoles = new ArrayList<>();
		List<Permiso> allPermisos = new ArrayList<>();
		Integer personId = (Integer) request.getSession().getAttribute("personId");

		try {
			allRoles = RolDao.selectAllRoles();
			allPermisos = PermisoDao.readAllPermisos();
		} catch (IllegalArgumentException | IOException e1) {
			e1.printStackTrace();
		}

		if (currentUser.isPermitted("usuarios:ver")) {
			try {
				request.getSession().setAttribute("usuarios", UsuarioFormDao.readTodosUsuarios());
				if (currentUser.isPermitted("roles:ver")) {
					request.getSession().setAttribute("roles", allRoles);// El conjunto de roles existentes
				}
//				if (currentUser.isPermitted("permisos:ver")) {
//					request.getSession().setAttribute("permisos", allPermisos);// El conjunto de permisos existentes
//				}
				System.out.println("CargaInicialAction.execute()");
				System.out.println(currentUser);
				System.out.println(currentUser.getPrincipal());
				System.out.println("===========================================");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			request.setAttribute("ErrorPermisos",
					"No cuentas con los suficientes permisos para visualizar los usuarios.");
		}
		return mapping.findForward("success");
	}

}
