package com.curso.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.exc.UnrecognizedPropertyException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.dao.PermisoDao;
import com.curso.util.SubjectUserInfo;

public class EditaPermiso extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		final Logger LOG = LoggerFactory.getLogger(EditaUsuario.class);// loggeo en consola
		
		System.out.println("EditaPermiso.execute()");
		System.out.println(ThreadContext.getSubject());
		System.out.println(ThreadContext.getSubject().getPrincipal());
		System.out.println("==============================="); 
		
		Subject currentUser = SubjectUserInfo.obtenerSubject(this.getServlet());
		System.out.println("===============================1");
		System.out.println(request.getSession().getAttribute("mail"));
		System.out.println(request.getSession().getAttribute("pass"));
		System.out.println("===============================1");
		SubjectUserInfo.loggear(SecurityUtils.getSubject(), request.getSession().getAttribute("mail")+"", request.getSession().getAttribute("pass")+"");
		System.out.println(currentUser);
		Session sesion = currentUser.getSession();
		
		if (currentUser.isPermitted("permisos:editar") ) {
			BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
			String x1 = br.readLine();
			x1 = x1.replace("\\", "");
			x1 = x1.substring(1, x1.length() - 1);
			JSONObject temporal = (JSONObject) new JSONParser().parse(x1);
			ObjectMapper om = new ObjectMapper();
			try {
				Permiso uf = om.readValue(temporal.toJSONString(), Permiso.class);
				if (PermisoDao.editarPermiso(uf)) {
					om.writeValue(response.getOutputStream(), om.convertValue(uf, Permiso.class));
					LOG.info("Se edita el permiso: " + uf.toString() + " exitosamente.");
				} else {
					om.writeValue(response.getOutputStream(), "Error");
					LOG.error("Error al editar permiso: " + uf.toString());
				}
				om.writeValue(response.getOutputStream(), om.convertValue(uf, Permiso.class));
			} catch (UnrecognizedPropertyException e) {
				om.writeValue(response.getOutputStream(), "Error");
				LOG.error("Error al editar permiso." + e.getMessage());
			}
		}else {
			sesion.setAttribute("mensaje", "No tienes acceso a esta seccion.");
			LOG.error("No tienes el permiso para editar permisos");
		}
		return null;
	}
}
