package com.curso.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.util.StdDateFormat;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.UsuarioForm;
import com.curso.model.formBeans.dao.UsuarioFormDao;
import com.curso.util.SubjectUserInfo;

public class GuardaUsuario extends Action {
	final Logger LOG = LoggerFactory.getLogger(this.getClass());
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LOG.info("GuardarUsuario.execute()");
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
		Subject currentUser = SubjectUserInfo.obtenerSubject(this.getServlet());
		String x = br.readLine();
		x = x.replace("\\", "");
		x = x.substring(1, x.length() - 1);

		JSONObject temporal = (JSONObject) new JSONParser().parse(x);
		ObjectMapper om = new ObjectMapper();
		UsuarioForm uf = om.readValue(temporal.toJSONString(), UsuarioForm.class);
		uf.setFechaRegistro(new Date(new java.util.Date().getTime()));
		uf.setFechaNacimiento(new Date(uf.getFechaNacimiento().getTime() + 86400000));
		if (currentUser.isPermitted("usuarios:agregar")) {
			if (UsuarioFormDao.guardarUsuario(uf)) {
				om.writeValue(response.getOutputStream(), om.convertValue(uf, UsuarioForm.class));
			} else {
				om.writeValue(response.getOutputStream(), "Error");
			}
		}else {
			om.writeValue(response.getOutputStream(), "No tienes permiso para guardar un usuario");
			LOG.error("No tienes permiso para guardar un usuario");
		}

		return null;
	}

	public static void main(String[] args)
			throws ParseException, JsonParseException, JsonMappingException, IOException, java.text.ParseException {
		JSONObject temporal = (JSONObject) new JSONParser().parse(
				"{\"fechaNacimiento\":\"2000-01-02\",\"apellido\":\"tt\",\"correo\":\"tt@tt.tt\",\"nombre\":\"tt\",\"edad\":\"34\"}");
		ObjectMapper om = new ObjectMapper();
		System.out.println(temporal.toJSONString());
		UsuarioForm uf = om.readValue(temporal.toJSONString(), UsuarioForm.class);
		uf.setFechaRegistro(new Date(new java.util.Date().getTime()));
		uf.setFechaNacimiento(new Date(uf.getFechaNacimiento().getTime() + 86400000));
		System.out.println(uf);
		System.out.println(new Date(new SimpleDateFormat("ddMMyyyy").parse("01012000").getTime() - 1));
		System.out.println(new StdDateFormat().parse("01012000"));

	}
}
