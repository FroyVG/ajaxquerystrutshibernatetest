package com.curso.action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import com.curso.model.formBeans.dao.UsuarioFormDao;
import com.curso.model.graficoPie.GrandGraficaPie;

public class GeneraGraficoPie extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		GrandGraficaPie graficaPie = new GrandGraficaPie(UsuarioFormDao.readTodosUsuarios());
		System.out.println(graficaPie);
		ObjectMapper om = new ObjectMapper();
		PrintWriter pw = response.getWriter();
		om.writeValue(pw, graficaPie);
		return null;
	}

}
