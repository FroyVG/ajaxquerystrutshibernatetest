package com.curso.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.curso.model.formBeans.Permiso;
import com.curso.model.formBeans.Rol;
import com.curso.model.formBeans.RolPermiso;
import com.curso.model.formBeans.dao.PermisoDao;
import com.curso.model.formBeans.dao.RolDao;
import com.curso.util.SubjectUserInfo;

public class AsignaPermisoByRol extends Action{
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String x = br.readLine();
		x = x.replace("\\", "");
		x = x.substring(1, x.length() - 1);
		JSONObject temporal = (JSONObject) new JSONParser().parse(x);
		JSONArray newIds =  (JSONArray) temporal.get("data");
		String rolName = (String) temporal.get("rol");
		
		Subject currentUser = SubjectUserInfo.obtenerSubject(this.getServlet());
		List<Permiso> allPermisos = new ArrayList<>();
		List<Permiso> permisosByRol = new ArrayList<Permiso>();//Los permisos que tenian anteriormente la cuenta.
		
		List<Long> idsList = new ArrayList<Long>();//Para obtener todos los de permisos la vista
		List<Permiso> newPermisos = new ArrayList<Permiso>();//Los nuevos permisos agregados en la vista
		List<Permiso> addedPermisos;//Para obtener los que fueron eliminados
		List<Permiso> removedPermisos;//Para obtener los permisos que fueron eliminados
		List<RolPermiso> rolPermisosListAdd = new ArrayList<RolPermiso>();
		List<RolPermiso> rolPermisosListRemove = new ArrayList<RolPermiso>();

		RolPermiso rolPermiso;
		Rol rol = RolDao.readRolByNombre(rolName);
		
		for (int i = 0; i < newIds.size(); i++) {
			idsList.add( (Long) newIds.get(i));
		}
		
		try {
			allPermisos = PermisoDao.readAllPermisos();
			permisosByRol = PermisoDao.readPermisosByRol(rolName);
		} catch (IllegalArgumentException | IOException e1) {
			e1.printStackTrace();
		}

		
		for (Long id : idsList) {
			for (Permiso permiso : allPermisos) {
				if(id == permiso.getId())
					newPermisos.add(permiso);
			}
		}
		addedPermisos  = new ArrayList<Permiso>(newPermisos);
		removedPermisos  = new ArrayList<Permiso>(permisosByRol);
		newPermisos.removeAll(permisosByRol);//Obtenemos los permisos agregados.
		removedPermisos.removeAll(addedPermisos);//Obtenemos los permisos eliminados
		
		for (int i = 0; i < removedPermisos.size(); i++) {
			LOG.info("removedPermisos("+i+"= " + removedPermisos.get(i)+")");
		}
		
		for (int i = 0; i < newPermisos.size(); i++) {
			LOG.info("newPermisos("+i+"= " + newPermisos.get(i)+")");
		}
		if (currentUser.isPermitted("roles:permisos:agregar")) {
			for (Permiso permiso : newPermisos) {//Agregar permisos a las lista de permisosRoles
				rolPermiso = new RolPermiso(0, permiso.getId(), rol.getId());
				rolPermisosListAdd.add(rolPermiso);
				}
			for (Permiso permiso2 : removedPermisos) {
				rolPermiso = new RolPermiso(0, permiso2.getId(), rol.getId());
				rolPermisosListRemove.add(rolPermiso);
			}
			PermisoDao.insertaPermisoRol(rolPermisosListAdd);
			PermisoDao.eliminaPermisoRol(rolPermisosListRemove);
//			request.getSession().removeAttribute("roles");
//			request.getSession().removeAttribute("permisos");
//			request.getSession().setAttribute("permisos", allPermisos);//El conjunto de permisos existentes
//			request.getSession().setAttribute("permisosByRol", permisosByRol);//El conjunto de permisos existentes
			LOG.info("{} Cuenta con los permisos para agregar permisos", currentUser.getPrincipal() );
		}else {
			LOG.warn("{} No cuenta con los permisos para agregar permisos", currentUser.getPrincipal() );
		}
		
		return super.execute(mapping, form, request, response);
	}
}
