<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="logic"
	uri="http://jakarta.apache.org/struts/tags-logic"%>
<%@ taglib prefix="bean" uri="http://jakarta.apache.org/struts/tags-bean"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="shiro" uri="/WEB-INF/tld/shiro.tld"%>

<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>AjaxQueryStrutsHibernateTest</title>

<link rel="styleSheet" type="text/css"
	href="/AjaxQueryStrutsHibernateTest/css/themes/jquery-ui-1.12.1.base/jquery-ui.css" />
<link rel="styleSheet" type="text/css"
	href="/AjaxQueryStrutsHibernateTest/css/index.css" />
<link rel="styleSheet" type="text/css"
	href="/AjaxQueryStrutsHibernateTest/css/bootstrap/bootstrap.min.css" />
<link rel="styleSheet" type="text/css"
	href="/AjaxQueryStrutsHibernateTest/css/bootstrap/bootstrap-datepicker.css" />
<link rel="styleSheet" type="text/css"
	href="/AjaxQueryStrutsHibernateTest/css/validationEngine.jquery.css" />
<link rel="stylesheet" type="text/css"
	href="/AjaxQueryStrutsHibernateTest/js/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css"
	href="/AjaxQueryStrutsHibernateTest/js/jquery.dataTables.min.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery-3.4.1.js"></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.ui.core.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.dataTables.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.dataTables.min.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.ui.widget.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.ui.position.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.ui.mouse.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.ui.draggable.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.ui.resizable.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.ui.dialog.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/dataTables.buttons.min.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jszip.min.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/pdfmake.min.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/vfs_fonts.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/buttons.html5.min.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.validationEngine-es.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/jquery.validationEngine.js" /></script>
<script type="text/javascript"
	src="https://code.highcharts.com/highcharts.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/bootstrap/bootstrap.js"></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/bootstrap-datepicker.js" /></script>
<script type="text/javascript"
	src="/AjaxQueryStrutsHibernateTest/js/bootstrap-datepicker.min.js" /></script>
<script type="text/javascript" charset="ISO-8859-1"
	src="/AjaxQueryStrutsHibernateTest/js/index.js" /></script>

<script type="text/javascript">

var matched, browser;

jQuery.uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
        /(msie) ([\w.]+)/.exec( ua ) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        [];

    return {
        browser: match[ 1 ] || "",
        version: match[ 2 ] || "0"
    };
};

matched = jQuery.uaMatch( navigator.userAgent );
browser = {};

if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if ( browser.chrome ) {
    browser.webkit = true;
} else if ( browser.webkit ) {
    browser.safari = true;
}

jQuery.browser = browser;
//$("document").ready( cargaInicial2); //La llamada a un m�todo no an�nimo desde ready debe ser sin parentesis.
</script>
<style type="text/css">
.login-form {
	width: 340px;
	margin: 50px auto;
}

.login-form form {
	margin-bottom: 15px;
	background: #f7f7f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
}

.login-form h2 {
	margin: 0 0 15px;
	color: black;
}

.form-control, .btn {
	min-height: 38px;
	border-radius: 2px;
}

.input-group-addon .fa {
	font-size: 18px;
}

.btn {
	font-size: 15px;
	font-weight: bold;
	margin-top: 15px;
}
</style>

</head>
<body onload="cargaInicial2()">
	<%--Formulario de login --%>
	<c:if test="${errorMensaje != null }">
		<h1>${errorMensaje}</h1>
	</c:if>
	<c:if test="${(Usuario == null )}">
		<div class="login-form">
			<form action="./Shiro" method="post">
				<h2 class="text-center">Ingrese sus datos</h2>
				<hr>
				<div>
					<label for="validationTooltip03" style="color: black;">Correo
						electr�nico:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text" id="validationTooltipAgePrepend"><i
								class="material-icons">mail</i></span>
						</div>
						<input type="email"
							class="form-control validate[required], custom[email], minSize[2], maxSize[100]"
							name="mail" id="mail" placeholder="Ingrese correo electr�nico"
							required>
					</div>
				</div>
				<div>
					<label for="validationTooltip01" style="color: black;">Contrase�a:
					</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text" id="validationTooltipNamePrepend"><i
								class="material-icons">vpn_key</i></span>
						</div>
						<input type="password"
							class="form-control validate[required], minSize[2], maxSize[100]"
							name="pass" id="pass" placeholder="Ingrese contrase�a" value=""
							required>
					</div>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-outline-primary btn-block">Iniciar sesi�n</button>
				</div>
			</form>
		</div>
	</c:if>
	<%--Formulario de login --%>

	<c:if test="${(errorMensaje == null )}">
		<c:if test="${(Usuario != null )}">
			<nav
				class="navbar navbar-dark fixed-top navbar-expand-lg bg-dark shadow">
				<a class="navbar-brand"
					href="${pageContext.request.contextPath}/index.jsp">Ejemplo de
					Datatable y HighCharts</a>
				<ul class="navbar-nav " style="margin-left: auto;">
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
						role="button" aria-haspopup="true" aria-expanded="false">Graficas</a>
						<div class="dropdown-menu">
							<shiro:hasPermission name="grafica:nacimiento:ver">
								<a class="dropdown-item" id="chartBirthdate" href="#">A�o de nacimiento</a>
							</shiro:hasPermission>
							<shiro:hasPermission name="grafica:registro:ver">
								<a class="dropdown-item" href="#">A�o de registro</a>
							</shiro:hasPermission>
						</div></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
						role="button" aria-haspopup="true" aria-expanded="false">Permisos</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" id="viewPermissions" href="#">Ver</a> 
							<a class="dropdown-item" id="newPermission" href="#">Agregar</a>
						</div></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
						role="button" aria-haspopup="true" aria-expanded="false">Roles</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" id="viewRoles" href="#">Ver</a> 
							<a class="dropdown-item" id="newRole" href="#">Agregar</a>
							<a class="dropdown-item" id="assingPermissions" href="#">Asignar Permisos</a>
						</div></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
						role="button" aria-haspopup="true" aria-expanded="false">Usuarios</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" id="newUser" href="#">Agregar</a>
							<a class="dropdown-item" id="assignRoles" href="#">Asignar roles</a>
						</div></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
						role="button" aria-haspopup="true" aria-expanded="false">${Usuario}</a>
						<div class="dropdown-menu">
							<a class="nav-link" id="logout" href="./logout" style="color: black;">Cerrar sesi�n</a>
						</div>
					</li>
				</ul>
			</nav>
			<div class="container-fluid">
				<main role="main" class="col-md-12 ml-sm-auto">
				<div id="tablePanel">
					<div id="alert"
						class="alert alert-success alert-dismissible fade show"
						role="alert">
						<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<h2>Tabla de usuarios</h2>
					<%--					<logic:notPresent name="usuarios" scope="session">
						<jsp:forward page="/logIn.jsp"></jsp:forward>
					</logic:notPresent>
					 --%>
					<shiro:hasPermission name="usuarios:editar">
						<div style="display: none" id="editar"></div>
					</shiro:hasPermission>
					<shiro:hasPermission name="usuarios:eliminar">
						<div style="display: none" id="eliminar"></div>
					</shiro:hasPermission>

					<shiro:hasPermission name="usuarios:ver">
						<center>
							<table class="stripe row-border" id="miTabla">
								<thead>
									<tr>
										<th>ID</th>
										<th>NOMBRE</th>
										<th>APELLIDO</th>
										<th>EDAD</th>
										<th>CORREO ELECTR�NICO</th>
										<th>FECHA DE NACIMIENTO</th>
										<shiro:hasPermission name="usuarios:eliminar">
											<th>ELIMINAR</th>
										</shiro:hasPermission>
										<shiro:hasPermission name="usuarios:editar">
											<th>EDITAR</th>
										</shiro:hasPermission>
									</tr>
								</thead>
								<tbody>
									<logic:iterate name="usuarios" id="elemento">
										<tr id="${elemento.id}">
											<td>${elemento.id}</td>
											<td>${elemento.nombre}</td>
											<td>${elemento.apellido}</td>
											<td>${elemento.edad}</td>
											<td>${elemento.correo}</td>
											<td>${elemento.fechaNacimiento}</td>
											<shiro:hasPermission name="usuarios:eliminar">
												<td><button class="btn btn-outline-danger"
														onclick="eliminar(${elemento.id})">
														<i class="material-icons">delete</i>
													</button></td>
											</shiro:hasPermission>
											<shiro:hasPermission name="usuarios:editar">
												<td><button class="btn btn-outline-primary"
														onclick="editarDialog(${elemento.id}, '${elemento.nombre}', '${elemento.apellido}',${elemento.edad}, '${elemento.correo }', '${elemento.fechaNacimiento }', '${elemento.fechaRegistro }')">
														<i class="material-icons">edit</i>
													</button></td>
											</shiro:hasPermission>
										</tr>
									</logic:iterate>
								</tbody>
							</table>
						</center>
					</shiro:hasPermission>
				</div>
				<div id="graficasAnioPanel">
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item"><a data-toggle="tab"
							class="nav-link active" href=#miGrafica style="color: black;">Barra</a></li>
						<li class="nav-item"><a data-toggle="tab" class="nav-link"
							href="#miGraficaPie" style="color: black;">Pastel</a></li>
					</ul>
					<div class="tab-content" style="width: 100%; float: left;">
						<div id="miGrafica" class="tab-pane fade in active"
							style="max-width: 100%; min-width: 100%; float: left;"></div>
						<div id="miGraficaPie" class="tab-pane fade"
							style="max-width: 100%; min-width: 100%; float: left;"></div>
					</div>
				</div>

 
				
				<%--Tabla para ver los permisos --%>
				<div id="divTablaPermisos">
					<%--Aqu� se llenar� la tabla con los permisos --%>
				</div>
				<%--Fin tabla para ver los permisos --%>
				
				<%--Tabla para ver los roles --%>
				<div id="divTablaRoles">
					<%--Aqu� se llenar� la tabla con los roles --%>
					<h2>Tabla de roles</h2>
					<shiro:hasPermission name="roles:editar">
						<div style="display: none" id="editarRol"></div>
					</shiro:hasPermission>
					<shiro:hasPermission name="roles:eliminar">
						<div style="display: none" id="eliminarRol"></div>
					</shiro:hasPermission>
					<center>
						<table class="stripe row-border" id="tablaRoles">
							<thead>
								<tr>
									<th>ID</th>
									<th>NOMBRE</th>
									<shiro:hasPermission name="roles:eliminar">
										<th>ELIMINAR</th>
									</shiro:hasPermission>
									<shiro:hasPermission name="roles:editar">
										<th>EDITAR</th>
									</shiro:hasPermission>
								</tr>
							</thead>
							<tbody>
								<logic:iterate name="roles" id="rol">
									<tr id="${rol.id}">
										<td>${rol.id}</td>
										<td>${rol.nombre}</td>
										<shiro:hasPermission name="roles:eliminar">
											<td><button class="btn btn-outline-danger"
													onclick="eliminarRol(${rol.id})">
													<i class="material-icons">delete</i>
												</button></td>
										</shiro:hasPermission>
										<shiro:hasPermission name="roles:editar">
											<td><button class="btn btn-outline-primary"
													onclick="editarRolDialog(${rol.id}, '${rol.nombre}')">
													<i class="material-icons">edit</i>
												</button></td>
										</shiro:hasPermission>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</center>
				</div>
				<%--Fin tabla para ver los roles --%>
				
				<%--Tabla para ver los permisos asignados a los roles--%>
				<div id="divTablaPermisosByRol">
					<h2>Tabla de Permisos asignados a los roles</h2>
					<center>
					<logic:present name="roles" scope="session">
						<form action="/AjaxQueryStrutsHibernateTest/ObtenerPermisosByRol.do" id="PermissionsByRol" method="post">
							<select name="rolesList" id="rolesList" onchange="getPermissionsByRol()" class="form-control form-control-lg">
							<option value="none" selected disabled>Seleciona un rol </option>
							<logic:iterate name="roles" id="rolSimple">
								<option value="${rolSimple.nombre}">${rolSimple.nombre}</option>
							</logic:iterate>
							</select>
						</form>
					</logic:present>
					<button type="submit" class="btn btn-light" onclick="guardarPermisosByRol()">Guardar cambios</button>
						<table class="stripe row-border" id="tablaPermisosByRol">
							<thead>
								<tr>
									<th>ID</th>
									<th>NOMBRE</th>
									<th>ASIGNADO</th>
								</tr>
							</thead>
							<tbody  id = "bodyPermisosByRol">
							</tbody>
						</table>
					</center>
				</div>
				<%--Fin tabla para ver los permisos asignados a los roles--%>
				
				<%--Tabla para ver los roles asignados a los usuarios--%>
				<div id="divTablaRolesByUser">
					<h2>Tabla de Permisos asignados a los roles</h2>
					<center>
					<logic:present name="users" scope="session">
						<form action="/AjaxQueryStrutsHibernateTest/ObtenerRolesByUser.do" id="RolesByUser" method="post">
							<select name="userList" id="userList" onchange="getRolesByUsers()" class="form-control form-control-lg">
							<option value="none" selected disabled>Selecciona un usuario </option>
							<logic:iterate name="users" id="userSimple">
								<option value="${rolSimple.nombre}">${rolSimple.nombre}</option>
							</logic:iterate>
							</select>
						</form>
					</logic:present>
					<button type="submit" class="btn btn-light" onclick="guardarRolesByUser()">Guardar cambios</button>
						<table class="stripe row-border" id="tablaRolesByUser">
							<thead>
								<tr>
									<th>ID</th>
									<th>NOMBRE</th>
									<th>ASIGNADO</th>
								</tr>
							</thead>
							<tbody  id = "bodyRolesByUser">
							</tbody>
						</table>
					</center>
				</div>
				<%--Fin tabla para ver los roles asignados a los permisos--%>  
				 
				</main>
			</div>
			<%-- Hasta aqu� es bootstrap -->

			<%--Formulario para guardar permisos --%>
			<div id="dialogGuardarPermisos" title="Insertar datos" class="modal"
				tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" style="color: black;">Datos para guardar nuevos permisos</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="formJQueryPermiso"
							class="validationEngineContainer">
							<form class="needs-validation">
								<fieldset>
									<div class="col-md-12">
										<label for="validationTooltip01" style="color: black;">Nombre:
										</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipNamePrependPermiso"><i
													class="material-icons">perm_device_information</i></span>
											</div>
											<input type="text"
												class="form-control validate[required], minSize[2], maxSize[100]"
												name="inputNombrePermiso" id="inputNombrePermiso"
												placeholder="Ingrese el nombre del permiso" value=""
												required>
											<div class="valid-tooltip">�Validaci�n correcta!</div>
										</div>
									</div>
									<input type="hidden" name="idPermiso" id="idPermiso">
								</fieldset>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary"
								data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-outline-primary"
								onclick="guardarPermiso()">Guardar</button>
						</div>
					</div>
				</div>
			</div>
			<%--Fin formulario para guardar permisos --%>
			
			<%--Formulario para guardar roles --%>
			<div id="dialogGuardarRoles" title="Insertar datos" class="modal"
				tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" style="color: black;">Datos para guardar nuevos roles</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="formJQueryRoles"
							class="validationEngineContainer">
							<form class="needs-validation">
								<fieldset>
									<div class="col-md-12">
										<label for="validationTooltip01" style="color: black;">Nombre:
										</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipNamePrependRoles"><i
													class="material-icons">perm_device_information</i></span>
											</div>
											<input type="text"
												class="form-control validate[required], minSize[2], maxSize[100]"
												name="inputNombreRol" id="inputNombreRol"
												placeholder="Ingrese el nombre del rol" value=""
												required>
											<div class="valid-tooltip">�Validaci�n correcta!</div>
										</div>
									</div>
									<input type="hidden" name="idRol" id="idRol">
								</fieldset>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary"
								data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-outline-primary"
								onclick="guardarRol()">Guardar</button>
						</div>
					</div>
				</div>
			</div>
			<%--Fin formulario para guardar roles --%>
			
			<%--Formulario para editar usuario --%>
			<div id="dialog" title="Modificar datos" class="modal" tabindex="-1"
				role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" style="color: black;">Editar datos
								usuario</h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="formJQuery"
							class="validationEngineContainer">
							<form class="needs-validation" id="formValidation">
								<fieldset>
									<div class="col-md-12">
										<label for="validationTooltip01" style="color: black;">Nombre:
										</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipNamePrepend"><i
													class="material-icons">person</i></span>
											</div>
											<input type="text" name="nombre" id="nombre"
												class="validate[required], minSize[2], maxSize[100]"
												placeholder="Ingrese el nombre del usuario" required>
											<div class="valid-tooltip">�Validaci�n correcta!</div>
										</div>
									</div>

									<div class="col-md-12">
										<label for="validationTooltip02" style="color: black;">Apellido:
										</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipFirstNamePrepend"><i
													class="material-icons">person</i></span>
											</div>
											<input type="text" name="apellido" id="apellido"
												class="validate[required], minSize[2], maxSize[100]"
												placeholder="Ingrese el apellido" required>
											<div class="valid-tooltip">�Validaci�n correcta!</div>
										</div>
									</div>

									<div class="col-md-12">
										<label for="validationTooltipUsername" style="color: black;">Edad:
										</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipAgePrepend"><i
													class="material-icons">filter_9_plus</i></span>
											</div>
											<input type="number" name="edad" id="edad"
												class="validate[required],custom[integer], min[1], max[100]"
												placeholder="Ingrese la edad" required>
											<div class="invalid-tooltip">Ingresa solo n�meros
												enteros</div>
										</div>
									</div>
									<div class="col-md-12">
										<label for="validationTooltip03" style="color: black;">Correo
											electr�nico:</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipAgePrepend"><i
													class="material-icons">mail</i></span>
											</div>
											<input type="email" name="correo" id="correo"
												class="validate[required], custom[email], min[1], max[100]"
												placeholder="Ingrese el correo electr�nico" required>
											<div class="invalid-tooltip">Ingresa un formato
												correcto 'usuario@dominio.com'.</div>
										</div>
									</div>
									<div class="col-md-12">
										<label for="validationTooltip04" style="color: black;">Fecha de Nacimiento:</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipAgePrepend"><i
													class="material-icons">today</i></span>
											</div>
											<input type="text" name="fechaNacimiento"
												id="fechaNacimiento" class="validate[required]"
												placeholder="Ingrese la fecha de nacimiento" readonly
												required>
											<div class="invalid-tooltip">Ingresa un fecha correcta.
											</div>
										</div>
									</div>
									<input type="hidden" name="fechaRegistro" id="fechaRegistro">
									<input type="hidden" name="id" id="id">
								</fieldset>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary"
								data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-outline-primary"
								onclick="editar()">Guardar</button>
						</div>
					</div>
				</div>
			</div>
			<%--Fin formulario para editar usuario --%>
			
			<%--Formulario para editar permiso --%>
			<div id="dialogEditarPermiso" title="Modificar datos" class="modal" tabindex="-1"
				role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" style="color: black;">Editar datos permiso</h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="formJQueryPermisos"
							class="validationEngineContainer">
							<form class="needs-validation" id="formValidationPermiso">
								<fieldset>
									<div class="col-md-12">
										<label for="validationTooltip01" style="color: black;">Nombre:
										</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipNamePrependPermisos"><i
													class="material-icons">person</i></span>
											</div>
											<input type="text" name="permisoNombre" id="permisoNombre"
												class="validate[required], minSize[2], maxSize[100]"
												placeholder="Ingrese el nombre del permiso" required>
											<div class="valid-tooltip">�Validaci�n correcta!</div>
										</div>
									</div>
									<input type="hidden" name="permisoId" id="permisoId">
								</fieldset>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary"
								data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-outline-primary"
								onclick="editarPermiso()">Guardar</button>
						</div>
					</div>
				</div>
			</div>
			<%--Fin formulario para editar permiso --%>
			
			<%--Formulario para editar Rol --%>
			<div id="dialogEditarRol" title="Modificar datos" class="modal" tabindex="-1"
				role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" style="color: black;">Editar datos rol</h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="formJQueryRol"
							class="validationEngineContainer">
							<form class="needs-validation" id="formValidationRol">
								<fieldset>
									<div class="col-md-12">
										<label for="validationTooltip01" style="color: black;">Nombre:
										</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"
													id="validationTooltipNamePrependRol"><i
													class="material-icons">person</i></span>
											</div>
											<input type="text" name="rolNombre" id="rolNombre"
												class="validate[required], minSize[2], maxSize[100]"
												placeholder="Ingrese el nombre del rol" required>
											<div class="valid-tooltip">�Validaci�n correcta!</div>
										</div>
									</div>
									<input type="hidden" name="rolId" id="rolId">
								</fieldset>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary"
								data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-outline-primary"
								onclick="editarRol()">Guardar</button>
						</div>
					</div>
				</div>
			</div>
			<%--Fin formulario para editar Rol --%>
			
			<%--Dialogo de confirmaci�n --%>
			<div id="confirmDialog" title="Dialogo de confirmaci�n">
				<p>�Est�s seguro que deseas continuar?</p>
			</div>
			<%--Dialogo de confirmaci�n --%>

		</c:if>
	</c:if>
</body>
</html>