<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="logic"
	uri="http://jakarta.apache.org/struts/tags-logic"%>
<%@ taglib prefix="bean"
	uri="http://jakarta.apache.org/struts/tags-bean"%>
<%@taglib prefix="shiro" uri="/WEB-INF/tld/shiro.tld"%>
<%@  taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<logic:iterate name="permisos" id="permiso">
	<tr id="${permiso.id}">
		<td>${permiso.id}</td>
		<td>${permiso.nombre}</td>
		<td><input type="checkbox"
			<logic:iterate name="permisosByRol" id="permisoByRol"> <c:if test="${ permiso.id == permisoByRol.id}"> checked </c:if> </logic:iterate> /><%--<logic:equal scope="session" value="${permisoByRol.id}" property="${permiso.id }" ></logic:equal> --%>
		</td>
	</tr>
</logic:iterate>

