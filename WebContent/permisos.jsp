<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="logic" uri="http://jakarta.apache.org/struts/tags-logic"%>
<%@ taglib prefix="bean" uri="http://jakarta.apache.org/struts/tags-bean"%>
<%@taglib prefix="shiro" uri="/WEB-INF/tld/shiro.tld"%>
<h2>Tabla de Permisos</h2>
<shiro:hasPermission name="permisos:editar">
	<div style="display: none" id="editarPermiso"></div>
</shiro:hasPermission>
<shiro:hasPermission name="permisos:eliminar">
	<div style="display: none" id="eliminarPermiso"></div>
</shiro:hasPermission>
<center>
	<table class="stripe row-border" id="tablaPermisos">
		<thead>
			<tr>
				<th>ID</th>
				<th>NOMBRE</th>
				<shiro:hasPermission name="permisos:eliminar">
					<th>ELIMINAR</th>
				</shiro:hasPermission>
				<shiro:hasPermission name="permisos:editar">
					<th>EDITAR</th>
				</shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
				<logic:iterate name="permisos" id="permiso">
					<tr id="${permiso.id}">
						<td>${permiso.id}</td>
						<td>${permiso.nombre}</td>
						<shiro:hasPermission name="permisos:eliminar">
							<td><button class="btn btn-outline-danger"
									onclick="eliminarPermiso(${permiso.id})">
									<i class="material-icons">delete</i>
								</button></td>
						</shiro:hasPermission>
						<shiro:hasPermission name="permisos:editar">
							<td><button class="btn btn-outline-primary"
									onclick="editarPermisoDialog(${permiso.id}, '${permiso.nombre}')">
									<i class="material-icons">edit</i>
								</button></td>
						</shiro:hasPermission>
					</tr>
				</logic:iterate>
		</tbody>
	</table>
</center>