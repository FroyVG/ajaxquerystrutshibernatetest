
function cargaInicial2(){

	$("#dialog").hide();
	$("#confirmDialog").hide();
	$("#graficasAnioPanel").hide();
	$("#alert").hide();
	$("#formJQuery > form").validationEngine();
	$("#formValidation").validationEngine();
	$("#dialogGuardarPermisos").hide();
	
	$("#tablaRoles").dataTable();
//	$("#tablaPermisosByRol").dataTable();
	$("#divTablaPermisos").hide();
	$("#divTablaRoles").hide();
	$("#divTablaPermisosByRol").hide();
	//JSON para traducir las opciones de la datatable a espa�ol
	var lang_ES = {"oLanguage": {
		"sProcessing" : "Procesando...",
		"sLengthMenu" : "Mostrar _MENU_ registros",
		"sZeroRecords" : "No se encontraron resultados",
		"sEmptyTable" : "Ning�n dato disponible en esta tabla =(",
		"sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
		"sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix" : "",
		"sSearch" : "Buscar:",
		"sUrl" : "",
		"sInfoThousands" : ",",
		"sLoadingRecords" : "Cargando...",
		"oPaginate" : {
			"sFirst" : "Primero",
			"sLast" : "�ltimo",
			"sNext" : "Siguiente",
			"sPrevious" : "Anterior"
		},
		"oAria" : {
			"sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending" : ": Activar para ordenar la columna de manera descendente"
		},
		"buttons" : {
			"copy" : "Copiar",
			"colvis" : "Visibilidad"
		}
	}};
	
	$(".dropdown-toggle").dropdown();
	//Fin DropDown Menu
	//DatePicker
	
	//$('table').dataTable($.extend(lang_ES, { buttons: [ 'copy', 'csv', 'excel', 'pdf' ] } ));
	//console.log($("#miTabla"));
	$("#miTabla").dataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf'
        ]
    });

	$("#inputFechaNacimiento").datepicker({
	    format: "yyyy-mm-dd",
	    clearBtn: true,
	    language: "es"
	});
	$("#fechaNacimiento").datepicker({
	    format: "yyyy-mm-dd",
	    clearBtn: true,
	    language: "es"
	});
	//Fin DatePicker
	cargaInicial();
	
}
	function cargaInicial(){
		
		$("#newUser").click(function(){
			console.log('new user');
			$('#dialogGuardar').modal('show');
			});
		$("#newPermission").click(function(){
			$("#dialogGuardarPermisos").modal('show');
		});
		$("#newRole").click(function(){
			$("#dialogGuardarRoles").modal('show');
		});
		$("#chartBirthdate").click(function(){
			//console.log("graficas");
			generaGrafico();
			$('#tablePanel').hide();
			$('#graficasAnioPanel').show();
			$('#miGrafica').addClass("show");
			$("#divTablaRoles").hide();
			$('#divTablaPermisos').hide();
			$("#divTablaPermisosByRol").hide();
			});
		$("#viewPermissions").click(function(){
			verPermisos($("#divTablaPermisos"));
			$('#tablePanel').hide();
			$('#graficasAnioPanel').hide();
			$('#divTablaRoles').hide();
			$('#divTablaPermisos').show();	
			$("#divTablaPermisosByRol").hide();
		});
		$("#viewRoles").click(function(){
			verRoles($("#divTablaRoles"));
			$('#tablePanel').hide();
			$('#graficasAnioPanel').hide();
			$('#divTablaRoles').show();
			$('#divTablaPermisos').hide();
			$("#divTablaPermisosByRol").hide();
		});
		$("#assingPermissions").click(function(){
			$('#tablePanel').hide();
			$('#graficasAnioPanel').hide();
			$('#divTablaRoles').hide();
			$('#divTablaPermisos').hide();
			$("#divTablaPermisosByRol").show();
		});
	}



function generaGrafico(parametro) {
		$.ajax({
			url : "/AjaxQueryStrutsHibernateTest/GeneraGraficoBarras.do",
			type : "POST",
			contentType: 'Content-type: application/json; charset=iso-8859-1',
			mimeType : "application/json",
			dataType : "json",
			data : JSON.stringify('{\"data\":\"' + parametro + '\"}'),
			success : function(resultado) {
				Highcharts.chart('miGrafica',{
					legend: {
				        reversed: true
				    },
				    plotOptions: {
				        series: {
				            stacking: 'normal'
				        }
				    },
					chart:{ type: 'column'},
					title: {text: 'Numero de personas nacidas por a�o'},
					subtitle: {text: 'De las personas que tenemos registradas'},
					tooltip:{
						headerFormat:'<span style="font-size: 20px">{point.key}</span> <table>',
						pointFormat:'<tr><td style="background-color:{series.color}; width:10px"></td> <td style="color:{series.color}"> {series.name}</td> <td style:"color:{series.color}"> {point.y}</td> ',
						footerFormat:'</table>',
						shared: true,
						useHTML: true
					},
					xAxis:{
						categories: resultado.categorias // Son todos los
															// posibles a�os
					},
					yAxis:{
						min:0,
						title:{
							text: 'Numero de personas por a�o'
						}
					},
					series: resultado.series // conjunto de todas la personas
												// existentes
				});
				$('.highcharts-credits').hide();
			},
			error : function(x, y, z) {
			},
			 beforeSend: function(jqXHR) {
			        jqXHR.overrideMimeType('application/json;charset=iso-8859-1');
			    }
		});
		
		// Generar gr�fica pie
		$.ajax({
			url : "/AjaxQueryStrutsHibernateTest/GeneraGraficoPie.do",
			type : "POST",
			contentType: 'Content-type: application/json; charset=iso-8859-1',
			mimeType : "application/json",
			dataType : "json",
			data : JSON.stringify('{\"data\":\"' + parametro + '\"}'),
			success : function(resultado) {
				//console.log(resultado);
				$('#miGraficaPie').css("height", $('#miGrafica').css("height"));
				// Obtener el tama�o para las graficas
				var width =$('#miGraficaPie').css("width");
				width = width.substring(0,width.length-2);
				var height=$('#miGraficaPie').css("height");
				height = height.substring(0,height.length-2);
				
				var graficasPie=[]; 
				for(var i =0 ; i < resultado.categorias.length; i++ ){
					graficasPie[i] = $('<div>');
					graficasPie[i].css("float","left");
					graficasPie[i].attr('id',"pie"+resultado.categorias[i]);
					graficasPie[i].appendTo($('#miGraficaPie'));
				}
				var referenciaCuadratica =Math.ceil(Math.sqrt($('#miGraficaPie > div').length));
				var totalCuadritos = referenciaCuadratica*referenciaCuadratica;
				$('#miGraficaPie > div').css("width",(width/referenciaCuadratica));
				$('#miGraficaPie > div').css("height",(height/(referenciaCuadratica - Math.floor((totalCuadritos%$('#miGraficaPie > div').length )/referenciaCuadratica ))));
				
				for(var i =0 ; i < resultado.categorias.length; i++ ){
				
				//console.log(resultado.graficasPie[i].data);
				
				Highcharts.chart("pie"+resultado.categorias[i],{
					legend: {
				        reversed: true
				    },
				    plotOptions: {
				        series: {
				            stacking: 'normal'
				        }
				    },
					chart:{ type: 'pie'},
					title: {
						text: 'Personas del a�o '+resultado.categorias[i],
						style: { 	font: (15/Math.ceil(Math.sqrt(resultado.categorias.length))+5) +'px Arial, sans-serif' 	}
					},
					subtitle: {text: 'Agrupadas',
						style: { 	font: (15/Math.ceil(Math.sqrt(resultado.categorias.length))+3)+'px Arial, sans-serif' 	}
						},
					series: 
						
						
						[{
				        name: resultado.categorias[i],
				        colorByPoint: true,
				        data: resultado.graficasPie[i].data
					}]
				});

				}
				$('.highcharts-credits').hide();
			},
			error : function(x, y, z) {
			},
			 beforeSend: function(jqXHR) {
			        jqXHR.overrideMimeType('application/json;charset=iso-8859-1');
			    }
		});
	}

function generaDiv(){
	
	var width =$('#graficaRegistro').css("width");
	width = width.substring(0,width.length-2);
	var height=$('#graficaRegistro').css("height");
	height = height.substring(0,height.length-2);
	
	
	
	var graficasPie = $('<div>');
	graficasPie.css("float","left");
	graficasPie.css("background-color","red");
	graficasPie.appendTo($('#graficaRegistro'));
	
	var referenciaCuadratica =Math.ceil(Math.sqrt($('#graficaRegistro > div').length));
	var totalCuadritos = referenciaCuadratica*referenciaCuadratica;
	//console.log(totalCuadritos+"-"+$('#graficaRegistro > div').length+"-"+referenciaCuadratica+"-"+(referenciaCuadratica - Math.floor((totalCuadritos%$('#graficaRegistro > div').length )/referenciaCuadratica )) );
	$('#graficaRegistro > div').css("width",(width/referenciaCuadratica));
	$('#graficaRegistro > div').css("height",(height/(referenciaCuadratica - Math.floor((totalCuadritos%$('#graficaRegistro > div').length )/referenciaCuadratica ))));
}

function guardar() {
	if ($("#formJQuery > form").validationEngine('validate')) {
		$
				.ajax({
					url : "/AjaxQueryStrutsHibernateTest/GuardaUsuario.do",
					type : "POST",
					contentType : "application/json",
					mimeType : "application/json",
					dataType : "json",
					data : JSON.stringify('{\"nombre\":\"'+ $("#inputNombre").val() + '\",'
							+ '\"apellido\":\"' + $("#inputApellido").val() + '\",' 
							+ '\"edad\":\"' + $("#inputEdad").val() + '\",'
							+ '\"correo\":\"' + $("#inputCorreo").val() + '\",'
							+ '\"fechaNacimiento\":\"' + $("#inputFechaNacimiento").val() + '\" }'),
					success : function(resultado) {
						if (resultado == "Error") {
							$('#dialogGuardar').modal('toggle');
							$('#alert').alert('close').html("No se pudo guardar el usuario: "
									+ resultado.nombre + " "
									+ resultado.apellido).show();
						}else if(resultado.includes("permiso")){
							$('#dialogGuardar').modal('toggle');
							//$('#alert').alert('close').html("No tienes permisos para guardar usuarios.").show();
							alert(resultado);
						} else {
							$('#dialogGuardar').modal('toggle');
							$('#alert').html("Se ha guardado exitosamente el usuario: "
									+ resultado.nombre + " "
									+ resultado.apellido).show();
							var table = $('table').DataTable();
							var temporal = table.row
									.add([
											resultado.id,
											resultado.nombre,
											resultado.apellido,
											resultado.edad,
											resultado.correo,
											resultado.fechaNacimiento,
											'<button class=\"btn btn-outline-danger\" onclick=\"eliminar('
													+ resultado.id
													+ ')\"><i class="material-icons">delete</i></button>',
											'<button class=\"btn btn-outline-primary\" onclick=\"editarDialog('
													+ resultado.id + ', \''
													+ resultado.nombre + '\', \''
													+ resultado.apellido+ '\', ' 
													+ resultado.edad + ', \'' 
													+ resultado.correo + '\', \''
													+ resultado.fechaNacimiento+'\')\"><i class="material-icons">edit</i></button>' ]);
							temporal.node().id = resultado.id;
							temporal.draw(false);
							$('input[type="text"]').val('');
						}
					},
					error : function(x, y, z) {
					}
				});
	}
}

function eliminar(parametro) {
	$.ajax({
		url : "/AjaxQueryStrutsHibernateTest/EliminaUsuario.do",
		type : "POST",
		contentType : "application/json",
		mimeType : "application/json",
		dataType : "json",
		data : JSON.stringify('{\"data\":\"' + parametro + '\"}'),
		success : function(resultado) {
			console.log(resultado);
			$('#alert').alert('close').html("Se ha eliminado exitosamente el usuario: "
					+ resultado.nombre + " "
					+ resultado.apellido).show();
			var table = $('table').DataTable();
			table.row('#' + parametro).remove().draw(false);
		},
		error : function(x, y, z) {
		}
	});
}

function editarDialog(id, nombre, apellido, edad, correo, fechaNacimiento, fechaRegistro) {
	$("#nombre").val(nombre);
	$("#apellido").val(apellido);
	$("#edad").val(edad);
	$("#correo").val(correo);
	$("#fechaNacimiento").val(fechaNacimiento);
	$("#fechaRegistro").val(fechaRegistro);
	$("#id").val(id);
	$("#dialog").modal();
}
function editar(){
	if ($("#formValidation").validationEngine('validate')) {
		$.ajax({
			url : "/AjaxQueryStrutsHibernateTest/EditaUsuario.do",
			type : "POST",
			contentType : "application/json",
			mimeType : "application/json",
			dataType : "json",
			data : JSON.stringify('{\"nombre\":\"'
							+ $("#nombre").val()
							+ '\",'
							+ '\"apellido\":\"'
							+ $("#apellido").val()
							+ '\",'
							+ '\"edad\":\"'
							+ $("#edad").val()
							+ '\", \"id\":\"'
							+ $("#id").val()
							+ '\", \"correo\":\"'+ $("#correo").val()
							+ '\", \"fechaNacimiento\":\"'+ $("#fechaNacimiento").val()
							+ '\", \"fechaRegistro\":\"'+ $("#fechaRegistro").val()
							+ '\"}'),
			success : function(resultado) {
				//console.log(resultado);
				if (resultado == "Error") {
					$('#dialog').modal('toggle');
					$('#alert').html("Error al editar el usuario.").show();
				} else {
					$('#dialog').modal('toggle');
					$('#alert').html("Se ha editado el usuario exitosamente ");
					
					var data = [
						resultado.id,
						resultado.nombre,
						resultado.apellido,
						resultado.edad,
						resultado.correo,
						resultado.fechaNacimiento ];
					
					if($('#eliminar').length!=0){
						var botonEliminar ='<button class=\"btn btn-outline-danger\" onclick=\"eliminar('
							+ resultado.id
							+ ')\"><i class="material-icons">delete</i></button>';
							data.push(botonEliminar);
					}
					if($('#editar').length!=0){
						var botonEditar ='<button class=\"btn btn-outline-primary\" onclick=\"editarDialog('
							+ resultado.id 			+ ', \''
							+ resultado.nombre 		+ '\', \''
							+ resultado.apellido 	+ '\', '
							+ resultado.edad 		+ ',\''
							+ resultado.correo 		+ '\',\''
							+ resultado.fechaNacimiento + '\',\''
							+ resultado.fechaRegistro 	+ '\')\"><i class="material-icons">edit</i></button>';
						data.push(botonEditar);
					}
					$('table')
							.dataTable().fnUpdate(
													data,
													$('table tr[id=' + resultado.id + ']')
												 );
				}
			},
			error : function(x, y, z) { }
		});
	}
}

function verPermisos(divTablaPermisos) {
	divTablaPermisos = $(divTablaPermisos);
	$.ajax({
		url : "/AjaxQueryStrutsHibernateTest/CargarPermisos.do",
		type : "POST",
		data : JSON.stringify('{}'),
		success : function(resultado) {
			divTablaPermisos.html(resultado);
			$("#tablaPermisos").dataTable();
		},
		error : function(x, y, z) {
		}
	});
}

function guardarPermiso() {
	if ($("#formJQueryPermiso > form").validationEngine('validate')) {
		$
				.ajax({
					url : "/AjaxQueryStrutsHibernateTest/GuardarPermiso.do",
					type : "POST",
					contentType : "application/json",
					mimeType : "application/json",
					dataType : "json",
					data : JSON.stringify('{\"nombre\":\"'+ $("#inputNombrePermiso").val() + '\"}'),
					success : function(resultado) {
						if (resultado == "Error") {
							$('#dialogGuardarPermisos').modal('toggle');
							$('#alert').alert('close').html("No se pudo guardar el permiso: "
									+ resultado.nombre).show();
						}else if(Object.values(resultado).includes("permiso")){
							$('#dialogGuardarPermisos').modal('toggle');
							//$('#alert').alert('close').html("No tienes permisos para guardar usuarios.").show();
							alert(resultado);
						} else {
							$('#dialogGuardarPermisos').modal('toggle');
							$('#alert').html("Se ha guardado exitosamente el permiso: "
									+ resultado.nombre ).show();
							var table = $('#tablaPermisos').DataTable();
							var temporal = table.row
									.add([
											resultado.id,
											resultado.nombre,
											'<button class=\"btn btn-outline-danger\" onclick=\"eliminarPermiso('
													+ resultado.id
													+ ')\"><i class="material-icons">delete</i></button>',
											'<button class=\"btn btn-outline-primary\" onclick=\"editarPermisoDialog('
													+ resultado.id + ', \''
													+ resultado.nombre + 
													'\')\"><i class="material-icons">edit</i></button>' ]);
							temporal.node().id = resultado.id;
							temporal.draw(false);
							$('input[type="text"]').val('');
						}
					},
					error : function(x, y, z) {
					}
				});
	}
}

function eliminarPermiso(parametro) {
	$.ajax({
		url : "/AjaxQueryStrutsHibernateTest/EliminarPermiso.do",
		type : "POST",
		contentType : "application/json",
		mimeType : "application/json",
		dataType : "json",
		data : JSON.stringify('{\"data\":\"' + parametro + '\"}'),
		success : function(resultado) {
			console.log(resultado);
			$('#alert').alert('close').html("Se ha eliminado exitosamente el usuario: "
					+ resultado.nombre + " "
					+ resultado.apellido).show();
			var table = $('table').DataTable();
			table.row('#' + parametro).remove().draw(false);
		},
		error : function(x, y, z) {
		}
	});
}

function editarPermisoDialog(id, nombre) {
	$("#permisoNombre").val(nombre);
	$("#permisoId").val(id);
	$("#dialogEditarPermiso").modal();
}
function editarPermiso(){
	if ($("#formValidationPermiso").validationEngine('validate')) {
		$.ajax({
			url : "/AjaxQueryStrutsHibernateTest/EditarPermiso.do",
			type : "POST",
			contentType : "application/json",
			mimeType : "application/json",
			dataType : "json",
			data : JSON.stringify('{\"nombre\":\"'
							+ $("#permisoNombre").val()
							+ '\", \"id\":\"'
							+ $("#permisoId").val()
							+ '\"}'),
			success : function(resultado) {
				console.log(resultado);
				if (resultado == "Error") {
					$('#dialogEditarPermiso').modal('toggle');
					$('#alert').html("Error al editar el permiso.").show();
				} else {
					$('#dialogEditarPermiso').modal('toggle');
					$('#alert').html("Se ha editado el permiso exitosamente ");
					
					var data = [
						resultado.id,
						resultado.nombre
						];
					
					if($('#eliminarPermiso').length!=0){
						var botonEliminar ='<button class=\"btn btn-outline-danger\" onclick=\"eliminarPermiso('
							+ resultado.id
							+ ')\"><i class="material-icons">delete</i></button>';
							data.push(botonEliminar);
					}
					if($('#editarPermiso').length!=0){
						var botonEditar ='<button class=\"btn btn-outline-primary\" onclick=\"editarPermisoDialog('
							+ resultado.id 			+ ', \''
							+ resultado.nombre 		+ '\')\"><i class="material-icons">edit</i></button>';
						data.push(botonEditar);
					}
					$('#tablaPermisos')
							.dataTable().fnUpdate(
													data,
													$('#tablaPermisos tr[id=' + resultado.id + ']')
												 );
				}
			},
			error : function(x, y, z) { }
		});
	}
}

function verRoles(divTablaRoles) {
	divTablaRoles = $(divTablaRoles);
	$.ajax({
		url : "/AjaxQueryStrutsHibernateTest/CargandoRoles.do",
		type : "POST",
		data : JSON.stringify('{}'),
		success : function(resultado) {
			//divTablaRoles.html(resultado);
		},
		error : function(x, y, z) {
		}
	});
}

function guardarRol() {
	if ($("#formJQueryRoles > form").validationEngine('validate')) {
		$
				.ajax({
					url : "/AjaxQueryStrutsHibernateTest/GuardarRol.do",
					type : "POST",
					contentType : "application/json",
					mimeType : "application/json",
					dataType : "json",
					data : JSON.stringify('{\"nombre\":\"'+ $("#inputNombreRol").val() + '\"}'),
					success : function(resultado) {
						if (resultado == "Error") {
							$('#dialogGuardarRoles').modal('toggle');
							$('#alert').alert('close').html("No se pudo guardar el rol: "
									+ resultado.nombre).show();
						}else if(Object.values(resultado).includes("permiso")){
							$('#dialogGuardarRoles').modal('toggle');
							//$('#alert').alert('close').html("No tienes permisos para guardar usuarios.").show();
							alert(resultado);
						} else {
							$('#dialogGuardarRoles').modal('toggle');
							$('#alert').html("Se ha guardado exitosamente el rol: "
									+ resultado.nombre ).show();
							var table = $('#tablaRoles').DataTable();
							var temporal = table.row
									.add([
											resultado.id,
											resultado.nombre,
											'<button class=\"btn btn-outline-danger\" onclick=\"eliminarRol('
													+ resultado.id
													+ ')\"><i class="material-icons">delete</i></button>',
											'<button class=\"btn btn-outline-primary\" onclick=\"editarRolDialog('
													+ resultado.id + ', \''
													+ resultado.nombre + 
													'\')\"><i class="material-icons">edit</i></button>' ]);
							temporal.node().id = resultado.id;
							temporal.draw(false);
							$('input[type="text"]').val('');
						}
					},
					error : function(x, y, z) {
					}
				});
	}
}
function editarRolDialog(id, nombre) {
	$("#rolNombre").val(nombre);
	$("#rolId").val(id);
	$("#dialogEditarRol").modal();
}
function editarRol(){
	if ($("#formValidationRol").validationEngine('validate')) {
		$.ajax({
			url : "/AjaxQueryStrutsHibernateTest/EditaRol.do",
			type : "POST",
			contentType : "application/json",
			mimeType : "application/json",
			dataType : "json",
			data : JSON.stringify('{\"nombre\":\"'
							+ $("#rolNombre").val()
							+ '\", \"id\":\"'
							+ $("#rolId").val()
							+ '\"}'),
			success : function(resultado) {
				console.log(resultado);
				if (resultado == "Error") {
					$('#dialogEditarRol').modal('toggle');
					$('#alert').html("Error al editar el rol.").show();
				} else {
					$('#dialogEditarRol').modal('toggle');
					$('#alert').html("Se ha editado el rol exitosamente ");
					
					var data = [
						resultado.id,
						resultado.nombre
						];
					
					if($('#eliminarRol').length!=0){
						var botonEliminar ='<button class=\"btn btn-outline-danger\" onclick=\"eliminarRol('
							+ resultado.id
							+ ')\"><i class="material-icons">delete</i></button>';
							data.push(botonEliminar);
					}
					if($('#editarRol').length!=0){
						var botonEditar ='<button class=\"btn btn-outline-primary\" onclick=\"editarRolDialog('
							+ resultado.id 			+ ', \''
							+ resultado.nombre 		+ '\')\"><i class="material-icons">edit</i></button>';
						data.push(botonEditar);
					}
					$('#tablaRoles')
							.dataTable().fnUpdate(
													data,
													$('#tablaRoles tr[id=' + resultado.id + ']')
												 );
				}
			},
			error : function(x, y, z) { }
		});
	}
}

function eliminarRol(parametro) {
	$.ajax({
		url : "/AjaxQueryStrutsHibernateTest/EliminarRol.do",
		type : "POST",
		contentType : "application/json",
		mimeType : "application/json",
		dataType : "json",
		data : JSON.stringify('{\"data\":\"' + parametro + '\"}'),
		success : function(resultado) {
			console.log(resultado);
			$('#alert').alert('close').html("Se ha eliminado exitosamente el usuario: "
					+ resultado.nombre + " "
					+ resultado.apellido).show();
			var table = $('table').DataTable();
			table.row('#' + parametro).remove().draw(false);
		},
		error : function(x, y, z) {
		}
	});
}

function getPermissionsByRol(){
	var form = $("#PermissionsByRol");
	var select = $("#rolesList")[0].selectedOptions[0].value;
	var bodyPermisosByRol = $("#bodyPermisosByRol")
	var tablaPermisosByRol = $("#tablaPermisosByRol");
//	form.attr("method","POST");
//	form.submit();
	$.ajax({
		url : form.attr('action'),
		type : form.attr('method'),
		data : JSON.stringify('{\"data\":\"' + select + '\"}'),
		success : function(resultado) {
			bodyPermisosByRol.html(resultado);
			tablaPermisosByRol.dataTable();
			//$("input[type=checkbox]").addClass("custom-control-input");
		},
		error : function(x, y, z) {
			console.log("Error Error Error");
		}
	});
}

function guardarPermisosByRol(){
	var table = $("#tablaPermisosByRol").DataTable();
	var ids = [];
	table.rows().every(function (rowIdx, tableLoop, rowLoop) {
	      var data = this.node();
		  if($(data).find('input').prop('checked')){
			  ids.push($(data).attr('id'));
			console.log($(data).attr('id'));
		}
	});
	var rol = $("#rolesList")[0].selectedOptions[0].value;
	
	$.ajax({
		url : "/AjaxQueryStrutsHibernateTest/AsignarPermisosByRol.do",
		type : 'POST',
		contentType: 'Content-type: application/json; charset=iso-8859-1',
		mimeType : "application/json",
		dataType : "json",
		data : JSON.stringify('{\"data\":[' + ids + '], \"rol\":\"'+rol+'\"}'),
		success : function(resultado) {
			bodyPermisosByRol.html(resultado);
			tablaPermisosByRol.dataTable();
			//$("input[type=checkbox]").addClass("custom-control-input");
		},
		error : function(x, y, z) {
			console.log("Error Error Error");
		}
	});
	
}